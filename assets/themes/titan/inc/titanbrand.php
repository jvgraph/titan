<?php
function titan_brand() {
echo "
<style>
	body{ background-color:#FFF;}
	body.login #login h1 a {
	background: url('".get_bloginfo('template_url')."/images/logo-sgv.png') 0px 0px no-repeat transparent;
	height:80px;
	width:320px;
	margin:auto; 
}
#wp-submit{
	background-color: #40b4e5;
}
</style>
";
}
add_action("login_head", "titan_brand");

function theme_my_url(){
    return 'http://titan360.com/'; //return to main site/homepage
}
add_filter('login_headerurl', 'theme_my_url');


add_action('admin_head', 'titan_logo');
function titan_logo() {
echo "
<style>
#header-logo { background: url('".get_bloginfo('template_url')."/images/logo-sgv.png') 0px 0px no-repeat transparent !important; }
</style>
";
}
// Custom WordPress Admin Color Scheme
function admin_css() {
	wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/css/admin.css' );
}
add_action('admin_print_styles', 'admin_css' );

add_action('wp_dashboard_setup', 'wpc_dashboard_widgets');
function wpc_dashboard_widgets() {
	global $wp_meta_boxes;
	// Today widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	// Last comments
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	// Incoming links
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	// Plugins
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
}