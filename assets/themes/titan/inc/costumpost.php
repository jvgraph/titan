<?php
/*
*
* @param: Post Types
*
*/
add_action('init', 'create_post_type');
function create_post_type() {
    register_post_type( 'titan_staff',
        array(
            'labels' => array(
                'name' => 'Titan Staffs',
                'singular_name' => 'Titan Staff',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Titan Staff',
                'edit' => 'Edit',
                'edit_item' => 'Edit Titan Staff',
                'new_item' => 'New Titan Staff',
                'view' => 'View',
                'view_item' => 'View Titan Staff',
                'search_items' => 'Search Titan Staffs',
                'not_found' => 'No Titan Staffs found',
                'not_found_in_trash' => 'No Titan Staffs found in Trash',
                'parent' => 'Parent Titan Staff'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
            'taxonomies' => array( '' ),
            //'menu_icon' => template_url( 'images/personel.png', __FILE__ ),
            'has_archive' => true
        )
    );
}
create_post_type();