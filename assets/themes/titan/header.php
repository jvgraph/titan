<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>
<?php
if(is_home()){
	bloginfo('name');
} elseif (is_category()){
	single_cat_title(); echo ' - ' ; bloginfo('name');
} elseif (is_single()){
	$customField =get_post_custom_values("title");
	if(isset($customField[0])){
		echo $customField[0];
	} else{
		single_post_title();
	}
} elseif(is_page()){
	bloginfo('name'); echo ': '; single_post_title();
} else {
	wp_title('', true);
}
?>
</title>
<?php wp_head(); 
if ( is_singular() ) { ?>
<link rel="canonical" href="<?php the_permalink(); ?>" /> 
<?php } ?>
</head>

<body <?php body_class('no-js'); ?>>
<div id="wrap"> <!-- wrapper begins -->
	<div id="site-brand">
		<header>
			<!--
<div class="brand col-lg-2 col-md-2 col-sm-3 col-xs-7">
				<div class="row">
					<h1 class=" col-lg-9 col-md-9 col-sm-6 col-xs-12">
						<a href="<?php bloginfo('url')?>" title="titan 360" class="brand-logo">
							<img src="<?php bloginfo('template_url'); ?>/images/titan.png" alt="<?php bloginfo('name'); ?>" width="375" height="151" /> 		TITAN
						</a>
					</h1>
				</div>
			</div>
-->
<div class="navbar navbar-default" role="navigation">
  <div class="container-fluid">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header clearfix">
           <h2 class="inline-block" id="brand">
      	 <a class="navbar-brand" href="<?php echo home_url(); ?>">
      	         <img src="<?php bloginfo('stylesheet_directory')?>/images/titan-outdoor.png" alt="<?php bloginfo('nane')?>" width="100" height="40" />
      	      </a>
      </h2>	
      <button type="button" class="navbar-toggle menu-trigger" data-toggle="collapse" data-target="#slide-menu">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar one"></span>
        <span class="icon-bar two"></span>
        <span class="icon-bar three"></span>
      </button>

    </div>

        <?php
            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
				'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
    </div>
</div>
</header>