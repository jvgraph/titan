<article <?php post_class(); ?>>
<?php
	if(!is_single() && !is_singular()){
	?> <h2>
		<a href="<? the_permalink() ?>"><?php the_title()?></a>
	</h2>
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<?php }else{
	the_title('<h1>', '</h1>', true);
	}
echo '</article>';