<?php get_header(); ?>
	</div>
	<div class="container-fluid">
		<?php
			if(have_posts()): while(have_posts()): the_post();
				get_template_part(PARTS, get_post_format() );
			endwhile; endif;
		?>
	</div>
<?php get_footer(); ?>
