<div id="footer">
	<footer class="container-fluid">
		<p class="site-info">
			&copy; Titan Outdoor, <?php echo date("Y"); ?>
		</p>
	</footer>
</div>
</div> <!-- END OF WRAPPER -->
<nav id="slide-menu">
	<ul>
		<h2 class="hide-text nav-title">site navigation</h2>
		<li class="timeline">company</li>
		<li class="events">media</li>
		<li class="calendar">markets</li>
		<li class="sep settings">titan print</li>
		<li class="logout">contact</li>
	</ul>
</nav>
<?php wp_footer(); ?>
</body>
</html>