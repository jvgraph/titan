<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page" id="sidebar">
	
	<div class="col-sidebar page-content">
		
		<header class="title">
			
			<h1>
				<?php echo get_the_title(); ?>
			</h1>
			
			<?php 

				if( is_page('news') || is_category('news') ): ?>
				<h2 class="subtitle">
					<?php the_field('subtitle'); ?>
				</h2>
			<?php endif; ?>
			
		</header>	
				
	</div> <!-- sidebar-content -->
	<span class="line-v">&nbs;</span>
</div>
