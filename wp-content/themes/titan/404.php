<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package titan
 */

get_header(); 


?>
<div class="container-fluid">

			<section class="error-404 not-found col-md-12 col-sm-12 col-xs-12">
				<div class="text-404">
				</div>
				<div class="404-img">
					<img src="<?php bloginfo('template_url')?>/images/404.svg" alt="404 error page titan outdoor" /> 
                                    </div>
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'dude, where’s my page?', 'titan' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'This page may have been moved or deleted. Please be sure to check your spelling.', 'titan' ); ?></p>

					
<div id="search" class="widget titan-seach-form search">

	<form role="search" action="<?php echo site_url('/'); ?>" method="get">

		<input type="search" name="s" class="search-input" value="<?php echo get_search_query(); ?>" placeholder="Search"/>

		<input type="submit" class="search-submit" value="Search" />
		<span class="icon icon-search sb-icon-search"></span>
	</form>

</div><!-- .search -->
					

				</div><!-- .page-content -->
			</section><!-- .error-404 -->
</div> <!-- container -->
<?php

 get_footer(); ?>
