<?php
	if( is_singular() ):
	
		the_title('<h1>','</h1>', true);
		
		else:
		echo '<h2><a href="'; 
		the_permalink(); 
		echo '">'; 
		the_title(); 
		echo '</a></h2>';
	endif;