<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page">
	<div class="col-sidebar page-content news">

		<header class="title"><h1><?php echo get_the_title(); ?></h1>
  <?php 
	  $subtile = get_field('subtitle');
	  if($subtile != ''):
  ?>

		<h2 class="news-subtitle"><?php  the_field('subtitle'); ?></h2> 
		  <?php  endif; ?>
		
		</header>			
	</div>
	<span class="line-v"></span>
</div>
		
<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 entry-content content-height">
	
	<div class="entry offices">
		<?php if( get_field('gallery') ){ ?>
	
						<div class="entry flexslider">
							<?php
							
								?>
								<ul class="media-slides slides clearfix">
									<?php
									 while( has_sub_field('gallery') ):
									 
									 $thumb = get_sub_field('images');
									 $img_url = $thumb["url"];
									 $image = aq_resize( $img_url, 910, 400, true );
									?>
									<li>
										
										<img src="<?php echo $image; ?>" />
									</li>
									<?php endwhile; ?>
								</ul>
						</div>
	<?php
		}
		else{
        ?>
		<div class="mythumbnail">
			<?php
			if ( has_post_thumbnail() ) {
			 	echo aq_resizer_img('full', 900, 400);
			 }
			?>
		</div>
		
		<?php }
			the_content(); 
		?>	
	</div>
	<div class="share">
		
<a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=<?php the_permalink(); ?>" target="_blank">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
<g>
	<rect fill="#CCCCCC" width="16" height="16"/>
	<path fill="#FFFFFF" d="M14,9.6l0.3-2.4h-2.4V5.6c0-0.7,0.2-1.2,1.2-1.2h1.3V2.2c-0.2,0-1-0.1-1.9-0.1c-1.9,0-3.1,1.1-3.1,3.2v1.8
		H7.2v2.4h2.1v6.3h2.5V9.6H14z"/>
</g>
</svg></a>

<a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=<?php the_permalink(); ?>" target="_blank"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
<g>
	<rect fill="#CCCCCC" width="16" height="16"/>
	<path fill="#FFFFFF" d="M13.3,4.7C12.9,4.9,12.5,5,12.1,5c0.5-0.3,0.8-0.7,1-1.2c-0.4,0.3-0.9,0.4-1.4,0.5c-0.4-0.4-1-0.7-1.6-0.7
		c-1.2,0-2.2,1-2.2,2.2c0,0.2,0,0.3,0.1,0.5C6.2,6.2,4.6,5.3,3.5,4C3.2,4.4,3.1,4.8,3.1,5.2c0,0.8,0.4,1.4,1,1.8
		c-0.4,0-0.7-0.1-1-0.3l0,0c0,1.1,0.8,1.9,1.8,2.1C4.7,8.9,4.5,9,4.3,9C4.1,9,4,9,3.9,8.9c0.3,0.9,1.1,1.5,2,1.5
		c-0.7,0.6-1.7,0.9-2.7,0.9c-0.2,0-0.4,0-0.5,0c1,0.6,2.1,1,3.4,1c4,0,6.2-3.3,6.2-6.2c0-0.1,0-0.2,0-0.3C12.7,5.5,13,5.1,13.3,4.7"
		/>
</g>
</svg></a>
<a href="https://api.addthis.com/oexchange/0.8/forward/linkedin/offer?url=<?php the_permalink(); ?>" target="_blank">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
<g>
	<rect fill="#CCCCCC" width="16" height="16"/>
	<path fill="#FFFFFF" d="M3.9,2.6c0.7,0,1.3,0.6,1.3,1.3c0,0.7-0.6,1.3-1.3,1.3c-0.7,0-1.3-0.6-1.3-1.3C2.6,3.2,3.1,2.6,3.9,2.6
		 M2.7,6.2H5v7.3H2.7V6.2z"/>
	<path fill="#FFFFFF" d="M6.4,6.2h2.2v1l0,0C8.9,6.6,9.6,6,10.7,6c2.3,0,2.7,1.5,2.7,3.5v4h-2.3V9.9c0-0.8,0-1.9-1.2-1.9
		C8.8,8,8.7,8.9,8.7,9.8v3.6H6.4V6.2z"/>
</g>
</svg>
</a>
<a href="https://api.addthis.com/oexchange/0.8/forward/google_plusone_share/offer?url=<?php the_permalink(); ?>" target="_blank">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
<g>
	<rect fill="#CCCCCC" width="16" height="16"/>
	<polygon fill="#FFFFFF" points="14,3.7 12.4,3.7 12.4,2 11.5,2 11.5,3.7 9.9,3.7 9.9,4.5 11.5,4.5 11.5,6.2 12.4,6.2 12.4,4.5 
		14,4.5 	"/>
	<path fill="#FFFFFF" d="M6.1,7c0.4,0,0.8-0.2,1-0.5c0.1-0.2,0.2-0.4,0.3-0.6c0-0.2,0-0.4,0-0.5c0-0.6-0.2-1.3-0.5-1.9
		C6.8,3.2,6.7,2.9,6.4,2.7C6.2,2.6,5.9,2.5,5.5,2.5S4.7,2.6,4.5,3C4.2,3.3,4.1,3.7,4.1,4.2c0,0.6,0.2,1.2,0.5,1.8
		C4.8,6.3,5,6.5,5.3,6.7C5.5,6.9,5.8,7,6.1,7 M8.4,11.7c0-0.4-0.1-0.8-0.4-1.1c-0.3-0.3-0.7-0.6-1.3-1c-0.1,0-0.2,0-0.4,0
		c-0.1,0-0.3,0-0.6,0c-0.3,0-0.7,0.1-1,0.2c-0.1,0-0.2,0.1-0.3,0.1C4.2,10,4,10.1,3.9,10.2c-0.1,0.1-0.3,0.3-0.4,0.5
		s-0.2,0.4-0.2,0.7c0,0.6,0.3,1,0.8,1.4c0.5,0.4,1.1,0.5,2,0.6c0.8,0,1.3-0.2,1.7-0.5C8.2,12.6,8.4,12.2,8.4,11.7 M7.7,2.5
		C7.8,2.6,8,2.7,8.1,2.8c0.1,0.1,0.2,0.3,0.4,0.5c0.1,0.2,0.2,0.4,0.3,0.6c0.1,0.2,0.1,0.5,0.1,0.8c0,0.6-0.1,1-0.4,1.4
		C8.3,6.2,8.2,6.3,8.1,6.5C7.9,6.6,7.8,6.8,7.6,6.9C7.5,7,7.4,7.1,7.3,7.2c0,0.2-0.1,0.3-0.1,0.5c0,0.2,0,0.3,0.1,0.4
		c0.1,0.1,0.2,0.2,0.2,0.3L8,8.8c0.5,0.3,0.8,0.6,1,0.9s0.4,0.8,0.4,1.3c0,0.8-0.3,1.5-1,2.1c-0.7,0.6-1.7,0.9-3.1,1
		c-1.1,0-1.9-0.2-2.5-0.7C2.2,13,2,12.4,2,11.8c0-0.3,0.1-0.6,0.3-1c0.2-0.4,0.5-0.7,1-1s1.1-0.5,1.6-0.6c0.6-0.1,1-0.1,1.4-0.1
		C6.1,9,6,8.8,5.9,8.6C5.8,8.5,5.8,8.3,5.8,8c0-0.1,0-0.3,0.1-0.4C5.9,7.5,6,7.4,6,7.3c-0.2,0-0.4,0-0.5,0C4.7,7.3,4,7,3.6,6.5
		C3,6.2,2.8,5.6,2.8,4.9c0-0.8,0.3-1.5,1-2.1c0.5-0.4,0.9-0.6,1.4-0.7C5.6,2,6.1,1.9,6.5,1.9h3.2l-1,0.6H7.7z"/>
</g>
</svg>
</a>
<a href="https://api.addthis.com/oexchange/0.8/forward/email/offer?url=<?php the_permalink(); ?>" target="_blank">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
<g>
	<rect fill="#CCCCCC" width="16" height="16"/>
	<rect x="2" y="4" fill="#FFFFFF" width="12" height="8"/>
	<polygon fill="#CCCCCC" points="8,10 3,6.3 3.6,5.5 8,8.8 12.4,5.5 13,6.3 	"/>
</g>
</svg></a>

	</div>
</div>
