<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 col-sidebar page-gal">
	
	<div class="page-content">
			<?php
				the_title('<h1 class="post-title">', '</h1>', true);
			?>
		<?php if( !is_page('markets')){ ?>
		
        <div class="entry tab accordion">
	        
            <h2 class='collapser'>
	            overview
            </h2>
            
	        <div class="entry-content">
		        <div class="bio collapse-content">
	        		<?php
				        the_content();
			        ?>
	        	</div>
	        </div>

			
				<?php  locate_template(array('parts/extra.php'), true, true); ?>      
    	</div><!-- end of overview -->  

        <?php }
	        if(is_page('markets')){ ?>
        	<div class="sub-nav">
	        	<?php
		        	wp_nav_menu( array(
					        'menu'              => 'market',
					        'theme_location'    => 'market',
					        'depth'             => 2,
					        'container'         => '',
					        'menu_class'        => 'media-nav',
					        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					        'walker'            => new wp_bootstrap_navwalker()
					        )
					    );
	        	?>
        	</div>
        <?php }
	        
	    ?>
	    
    </div>
      <?php if( is_page('careers') ): ?>
    
<div class="opening-nav sub-nav">
	<ul>
		<li>
	  
	  
	      	<a href="" style="font-size: 1.5em;">openings <span>
		      	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="20.9px" height="20.6px" viewBox="0 0 20.9 20.6" enable-background="new 0 0 20.9 20.6" xml:space="preserve">
<polyline fill="none" stroke="#CCCCCC" stroke-width="1.45" stroke-linecap="square" stroke-miterlimit="10" points="8.3,16.1 
	14.2,10.2 8.3,4.2 "/>
</svg>
</span></a>
	      	
	   </li>
	</ul>
</div>
<?php endif; ?>		
	<span class="line-v"></span>

</div>


<div <?php post_class('col-lg-9 col-md-8 col-sm-8 col-xs-12 entry-content content-height')?>>
	
	<?php 
		if( is_page( array( 117, 'our-work') ) ): ?>
	<div class="our-work row">
		  <?php 
			  $images = get_field('image_gallery');
			  
			  if( $images ):
		  ?>	
		  <ul id="work">
			  <?php foreach( $images as $img ){ ?>
			  <li class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
			    <a href="<?php echo $img['url']; ?>" class="gallery">
				      <?php 
					      
					        //$img_path = aq_resize($img['url'], 400, 320, true);
					        
					        $params = array( 'width' => 400, 'height' => 320, 'crop' => true );
							$image = bfi_thumb( $img['url'], $params );
							
				      ?>
				     <img src='<?php echo $image; ?>' alt="<?php echo $img["alt"]; ?>"/>
				    
				    <span class="title"><?php echo $img['title']; ?></span>
				 </a>
			  </li>
			  <?php 
				  } 
			  ?>
		  </ul>
		    <?php endif; ?>
	</div>

	<?php endif; 
		
		if( !is_page( array( 117, 'our-work') ) ):
	?>
	
	
	  <?php if( get_field('gallery') && has_post_format('gallery') ){ ?>
	
	<div class="entry flexslider">
		<?php if( is_page( array('links', 1557) ) ): ?>
		<div class="award">
			<h1>got it</h1>
		</div>
		<?php endif; ?>
			<ul class="media-slides slides clearfix">
				<?php
				 while( has_sub_field('gallery') ):
				?>
				<li>
					<img src="<?php $image = get_sub_field('images'); echo $image["url"]; ?>"/>
				</li>
				<?php endwhile; ?>
			</ul>
	</div>

	<?php
		}
		elseif( has_post_format('gallery') ){
        	echo $gallery;
        }else{
	        if( get_field('page_sub_title') ){
		        ?>
		        <h4 class="subtitle page-sub"><?php the_field( 'page_sub_title' ); ?></h4>
		        <?php
	        }
	        echo aq_resizer_img('full', 1100, 960);
        }
        endif;
      ?>
</div>	