<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 page">
    <div class="page-content">
	<?php
		the_title('<h1 class="post-title">', '</h1>', true);
	?>
        <div class="entry tab accordion">
            <h2 class='collapser'>about</h2>
	        <div class="entry-content">
		        <div class="bio collapse-content">
	        		<?php the_content(); ?>
	        	</div>
	        </div>
        </div>
    </div>
    <span class="line-v"></span>
</div>
<div <?php post_class('col-lg-9 col-md-8 col-sm-8 col-xs-12 content-height') ?>>
	<?php
	if ( has_post_thumbnail() ) {
	 	echo aq_resizer_img('full', 700, 700);
	 }
	?>
</div>