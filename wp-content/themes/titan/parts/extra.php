<?php
        if( have_rows('available_media') ):

     ?>

            <h2 class='collapser'>availability</h2>
	        <div class="entry-content">
		        <?php while ( have_rows('available_media') ) : the_row(); ?>
		        <div class="bio collapse-content">
	        		<h3>
		        		<?php the_sub_field('media_title');	 ?>
	        		</h3>
	        			<p><?php the_sub_field('media_description'); ?></p>		        
	        	</div>
	        	<?php   endwhile; ?>
	        </div>
          
<?php
	endif; 


if( have_rows('available_market') ):

			// loop through the rows of data
     		?>

            <h2 class='collapser'>available media</h2>
	        <div class="entry-content">
		        <?php while ( have_rows('available_market') ) : the_row(); ?>
		        <div class="bio collapse-content">
	        		<h3>
		        		<?php the_sub_field('media_title');	 ?>
	        		</h3>
	        			<p><?php the_sub_field('media_description'); ?></p>		        
	        	</div>
	        	<?php   endwhile; ?>
	        </div> 
 <?php
  

endif;
?>