<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col-sidebar page-gal">
	
	<div class="page-content">
		<?php
			the_title('<h1 class="post-title">', '</h1>', true);
		?>
	        <div class="entry tab accordion">
		        
	            	<h2 class='collapser'>overview</h2>
	            	
			        <div class="entry-content">
				        
				        <div class="bio collapse-content">
			        		<?php
						        the_content();
					        ?>
			        	</div>
			        	
			        </div>
			        
		          <?php locate_template(array('parts/extra.php'), true, true); ?>
	        </div>    
    </div>
    
	<span class="line-v"></span>
	
</div>  


<div <?php post_class('col-lg-9 col-md-9 col-sm-8 col-xs-12 content-height')?>>
	
	<?php if( get_field('gallery') && has_post_format('gallery') ){ ?>
	
	<div class="entry flexslider">
		<?php if( is_page( array('links', 1557) ) ): ?>
		<div class="award" id="aww">
			<img src="<?php bloginfo('template_url'); ?>/images/award.svg" width="89" height="auto" class="awww" />
		</div>
		<?php endif; ?>
			<ul class="media-slides slides clearfix">
				<?php
				 while( has_sub_field('gallery') ):
				?>
				<li>
					<img src="<?php $image = get_sub_field('images'); echo $image["url"]; ?>"/>
				</li>
				<?php endwhile; ?>
			</ul>
	</div>
	<?php
		}
		elseif( has_post_format('gallery') ){
        	echo $gallery;
        }else{
	        echo aq_resizer_img('full', 950, 624);
        }
      ?>
</div>

	