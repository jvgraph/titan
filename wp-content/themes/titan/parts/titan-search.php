<div id="sb-search" class="widget titan-seachform search sb-search">

	<form role="search" action="<?php echo site_url('/'); ?>" method="get" class="clearfix form-master">

		<input type="search" name="s" class="sb-search-input" value="<?php echo get_search_query(); ?>" placeholder="Search"/>

		<input type="submit" class="sb-search-submit" value="Search" />
		<span class="icon icon-search sb-icon-search"></span>
	</form>

</div><!-- .search -->