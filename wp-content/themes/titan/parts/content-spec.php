<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 col-sidebar">
	<div class="page-content">
		
		<?php the_title('<h1 class="post-title">', '</h1>', true); ?>
		
        <div class="entry tab accordion">
            
            <h2 class='collapser'>overview</h2>
	        
	        <div class="entry-content">
		        <div class="bio collapse-content">

	        	</div>
	        
	        </div>
	        
        </div>
        
  	</div>
  		
	<span class="line-v"></span>

</div> <!-- end of sidebar -->

<div <?php post_class('col-lg-9 col-md-8 col-sm-8 col-xs-12 content-height')?>>
	<ul class="list">
	<?php if( have_rows('specs_files') ): ?>

	

	<?php while( have_rows('specs_files') ): the_row(); 

		// vars
		$new_spec = get_sub_field('new_spec_file');
		$spec_title = get_sub_field('specs_file_name');
		$spec_link = get_sub_field('spec_file_url');

		?>

		<li class="spec-file">

			<?php if( $new_spec != ''){ ?>
				<a href="<?php echo $new_spec; ?>"> <?php the_field('specs_file_name'); ?></a>
			<?php 
				}else{
			?>
			<a href="<?php echo $spec_link; ?>">
				<?php the_field('specs_file_name'); ?>
			</a>
			<?php } ?>
		</li>

	<?php endwhile; ?>



<?php endif; ?>
	</ul>
</div>
	