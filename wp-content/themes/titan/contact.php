<?php
/*
*
* Template Name: Req quote
*
*/
get_header();

	locate_template(array('html-parts/html-before.php'), true, true);
	?>
	
<?php	
	if(have_posts()): while(have_posts()): the_post();
	?>
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page">
			<div class="col-sidebar page-content">
				<header class="title"><h1><?php the_title(); ?></h1></header>
				<div class="sub-nav">
					<?php
					    wp_nav_menu( array(
					        'menu'              => 'titan-print',
					        'theme_location'    => 'titan-print',
					        'depth'             => 2,
					        'container'         => '',
					        'menu_class'        => 'ask-titan-nav',
					        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					        'walker'            => new wp_bootstrap_navwalker())
					    );
					?>
				</div>
			</div>
			<span class="line-v"></span>
		</div>
		
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 content-height">
			
			<div class="entry offices">
				<form method="post" action="send.php" class="contact-form">
					<input type="text" class="name" name="Name" id="Name" placeholder="your name" />
					
					<input type="text" class="subj" name="subject" id="subject" placeholder="subject" />
					
					<textarea name="Message" class="msg" rows="20" cols="20" id="Message" placeholder="job specifications"></textarea>
	
					<input type="submit" name="submit" value="request quote" class="submit-button" />
				</form>
			</div>
			
		</div>
	<?php	
	endwhile; endif;
	
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();