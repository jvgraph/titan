<?php
/*
	Template Name: full width page
*/
get_header();

	locate_template(array('html-parts/html-before.php'), true, true);
	
	if(have_posts()): while(have_posts()): the_post();
		
		the_content();
		
	endwhile; endif;
	
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();