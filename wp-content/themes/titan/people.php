<?php 
/*
*
* Template Name: People Page
*
*/
get_header();
			
		get_template_part(HTML, 'before');
		?>
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page">
            <div class="page-content">
	<?php
		the_title('<h1 class="post-title">', '</h1>', true);
	?>
	<?php if( is_page('our-people') ){ ?>
        <div class="entry tab accordion">
            <h2 class='collapser'>overview</h2>
            <?php if( have_posts() ): while( have_posts() ): the_post(); ?>
	        <div class="entry-content">
		        <div class="bio collapse-content">
	        		<?php
				        the_content();			
			        ?>
	        	</div>
	        </div>
	        <?php  endwhile; endif;
		        wp_reset_query();
	        ?>
        </div>

        <div class="sub-nav">
					<?php
					    wp_nav_menu( array(
					        'menu'              => 'people-menu',
					        'theme_location'    => 'people-menu',
					        'depth'             => 2,
					        'container'         => '',
					        'menu_class'        => 'ask-titan-nav',
					        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					        'walker'            => new wp_bootstrap_navwalker())
					    );
					?>
		</div>
        <?php } ?>
    </div>
 <span class="line-v"></span>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 staff content-height">
			<div class="row">
				<?php if( !is_page('our-people' )){ ?>
				<ul id="people" class="clearfix">
				<?php
					$args = array(
					'post_type' => 'people',
					'posts_per_page' => -1,
					'order' => 'ASC'				
					);
					$people = new WP_Query($args);
					
					if( $people->have_posts() ): while( $people->have_posts() ): $people->the_post(); ?>
					    <li <?php post_class("col-lg-3 col-md-3 col-sm-4 col-xs-6 person")?>>
							<div class="person-inner">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php
						if ( has_post_thumbnail() ) {
						 	echo aq_resizer_img('full', 360, 370);
						 }else{
							 echo get_the_attached_image();
						 }						
						?>
						<div class="person-info">
							<h3 class="name">
								<?php the_title();?>
								<span class="position">
								<?php $person_headline = esc_html(get_post_meta($post->ID, 'person_headline', true)); 
									if($person_headline != ''){
										echo $person_headline;
									}
								?>
								</span>
							</h3>
							<span class="plus">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="30.8px" height="30.9px" viewBox="0 0 30.8 30.9" enable-background="new 0 0 30.8 30.9" xml:space="preserve">
<g>
	<path fill="#FFFFFF" d="M15.4,0.3C7,0.3,0.2,7.1,0.2,15.5c0,8.4,6.8,15.2,15.2,15.2c8.4,0,15.2-6.8,15.2-15.2
		C30.6,7.1,23.8,0.3,15.4,0.3z M15.4,29.2C7.8,29.2,1.7,23,1.7,15.5c0-7.5,6.1-13.7,13.7-13.7s13.7,6.1,13.7,13.7
		C29.1,23,22.9,29.2,15.4,29.2z"/>
	<polygon fill="#FFFFFF" points="16.1,6.7 14.6,6.7 14.6,14.7 6.6,14.7 6.6,16.2 14.6,16.2 14.6,24.3 16.1,24.3 16.1,16.2 
		24.2,16.2 24.2,14.7 16.1,14.7 	"/>
</g>
<text transform="matrix(1 0 0 1 398.666 308.5635)"><tspan x="0" y="0" font-family="'MyriadPro-Regular'" font-size="12">&lt;svg version=&quot;1.1&quot; id=&quot;Layer_1&quot; xmlns=&quot;http://www.w3.org/2000/svg&quot; xmlns:xlink=&quot;http://www.w3.org/1999/xlink&quot; x=&quot;0px&quot; y=&quot;0px&quot;</tspan><tspan x="0" y="14.4" font-family="'MyriadPro-Regular'" font-size="12" letter-spacing="33">	</tspan><tspan x="36" y="14.4" font-family="'MyriadPro-Regular'" font-size="12"> width=&quot;25.4px&quot; height=&quot;25.8px&quot; viewBox=&quot;0 0 25.4 25.8&quot; enable-background=&quot;new 0 0 25.4 25.8&quot; xml:space=&quot;preserve&quot;&gt;</tspan><tspan x="0" y="28.8" font-family="'MyriadPro-Regular'" font-size="12">&lt;g&gt;</tspan><tspan x="0" y="43.2" font-family="'MyriadPro-Regular'" font-size="12" letter-spacing="33">	</tspan><tspan x="36" y="43.2" font-family="'MyriadPro-Regular'" font-size="12">&lt;polygon fill=&quot;#FFFFFF&quot; points=&quot;13.1,6.7 12.4,6.7 12.4,12.4 6.6,12.4 6.6,13.2 12.4,13.2 12.4,18.9 13.1,18.9 13.1,13.2 </tspan><tspan x="0" y="57.6" font-family="'MyriadPro-Regular'" font-size="12" letter-spacing="33">		</tspan><tspan x="72" y="57.6" font-family="'MyriadPro-Regular'" font-size="12">18.9,13.2 18.9,12.4 13.1,12.4 </tspan><tspan x="212.8" y="57.6" font-family="'MyriadPro-Regular'" font-size="12">	</tspan><tspan x="216" y="57.6" font-family="'MyriadPro-Regular'" font-size="12">&quot;/&gt;</tspan><tspan x="0" y="72" font-family="'MyriadPro-Regular'" font-size="12" letter-spacing="33">	</tspan><tspan x="36" y="72" font-family="'MyriadPro-Regular'" font-size="12">&lt;path fill=&quot;#FFFFFF&quot; d=&quot;M12.8,0.8C6,0.8,0.5,6.3,0.5,13S6,25.3,12.8,25.3S25,19.8,25,13S19.5,0.8,12.8,0.8z M12.8,24.5</tspan><tspan x="0" y="86.4" font-family="'MyriadPro-Regular'" font-size="12" letter-spacing="33">		</tspan><tspan x="72" y="86.4" font-family="'MyriadPro-Regular'" font-size="12">C6.4,24.5,1.3,19.3,1.3,13c0-6.3,5.1-11.5,11.5-11.5c6.3,0,11.5,5.1,11.5,11.5C24.2,19.3,19.1,24.5,12.8,24.5z&quot;/&gt;</tspan><tspan x="0" y="100.8" font-family="'MyriadPro-Regular'" font-size="12">&lt;/g&gt;</tspan><tspan x="0" y="115.2" font-family="'MyriadPro-Regular'" font-size="12">&lt;/svg&gt;</tspan></text>
</svg>
							</span>
							
						</div>
						</a>
						</div>
						<?php						
					echo '</li>';
				
					endwhile; endif;
				?>
			</ul>
			<?php }else{
					if(has_post_thumbnail()){
						echo aq_resizer_img('full', 1100, 960);
					}
				}
			?>
		</div>
	</div>
	<?php
	get_template_part(HTML, 'after');

get_footer(); 

?>