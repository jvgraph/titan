<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>
<?php
if(is_home()){
	bloginfo('name');
} elseif (is_category()){
	single_cat_title(); echo ' - ' ; bloginfo('name');
} elseif (is_single()){
	$customField = get_post_custom_values("title");
	if(isset($customField[0])){
		echo $customField[0];
	} else{
		single_post_title();
	}
} elseif(is_page()){
	bloginfo('name'); echo ': '; single_post_title();
} else {
	wp_title('', true);
}
?>
</title>
<?php 
	if( is_page_template('ask.php')):
if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
        wpcf7_enqueue_scripts();
    } 
if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
    wpcf7_enqueue_styles();
}
endif;

	wp_head(); ?>
</head>

<body <?php body_class('no-js clearfix'); ?>>
	
	
<div id="site-brand" class="navbar navbar-default navbar-fixed-top wrap">
	
 <header>
	 
   <div class="container-fluid">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header clearfix">
           <h2 class="inline-block" id="brand">
	      	  <a class="navbar-brand" href="<?php echo home_url(); ?>">
	      	   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="100px" height="40px" viewBox="0 0 100 40" enable-background="new 0 0 100 40" xml:space="preserve">
<path fill="#40B4E5" d="M58.6,22.4h4.2l-2.1-6.5h-0.1L58.6,22.4z M87.5,29.3h-5l-6.5-11.6H76v11.6h-4.5V10.7h5l6.5,11.4H83V10.7h4.5  V29.3z M70.1,29.3h-5L63.9,26h-6.5l-1.2,3.3h-5l6.9-18.6h4.9L70.1,29.3z M53.5,14.9h-5.5v14.4h-4.8V14.9h-5.5v-4.2h15.9V14.9z   M35.4,29.3h-4.8V10.7h4.8V29.3z M28.4,14.9h-5.5v14.4H18V14.9h-5.5v-4.2h15.9V14.9z M100,0H0v40h100V0z"/>
</svg>
	      	  </a>
	  	  </h2>		
		<button type="button" class="navbar-toggle menu-trigger" data-toggle="collapse" data-target="#slide-menu">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar one"></span>
		  <span class="icon-bar two"></span>
		  <span class="icon-bar three"></span>
		</button>
      				
      <?php get_template_part('parts/titan', 'search'); ?>
       </div>
   </div>
   
 </header>
 
</div>



<!--
<?php if( !is_home() AND !is_front_page() ): ?>
<a href="javascript:history.go(-1)" class="button back">&lt; back</a>
<?php endif; ?>
-->
<?php if( !is_404() AND !is_home() AND !is_front_page() AND !is_singular("people") AND !in_category('news') ): ?>
<!-- ************************* -->
<div class="breadcrumbs-wrap clearfix"><!-- BREADCRUMBS -->
	<div class="container-fluid">
	<?php
		echo titan_breadcrumbs();
	?>
	</div>
</div> <!-- BREADCRUMBS -->
<!-- ************************* -->
<?php endif; 
	
	if( is_singular( 'people' ) ){
		?>
	<div class="breadcrumbs-wrap clearfix"><!-- BREADCRUMBS -->
		<div class="container-fluid">
			<div id="crumbs">
				<a href="<?php bloginfo('url'); ?>">home </a>  
				<a href="<?php bloginfo('url'); ?>/company/">company</a>  
				<a href="<?php bloginfo('url'); ?>/company/our-people/">our people</a> 
				<a href="<?php bloginfo('url'); ?>/company/our-people/executive-team/">executive team</a> 
				<span class="current"><?php echo get_the_title(); ?></span>
			</div>
		</div>
	</div>
		<?php
	}
?>
<!-- NEWS PAGE -->
<?php
if( is_category('news') || in_category('news') ):
?>
<div class="breadcrumbs-wrap clearfix"><!-- BREADCRUMBS -->
<div class="container-fluid">
		<div id="crumbs"><a href="<?php bloginfo('url'); ?>">home </a>  <a href="<?php bloginfo('url'); ?>/company/">company</a>  <a href="<?php bloginfo('url'); ?>/company/news/">news</a> <span class="current"><?php echo short_title( '...', 20 ); ?></span></div>
</div>
	</div>
<?php endif; ?>

<div id="wrap" class="wrap"> <!-- wrapper begins -->
