<?php
/*
*
* Template Name: Specs Landing Page
*
*/
get_header();
locate_template(array('html-parts/html-before.php'), true, true);
?>

		<div class="col-sm-12 col-sm-4 col-md-3 col-lg-3 col-xl-2" id="spec-nav">

            <div class="page-content col-sidebar">
	        <header role="header">
                <h1 class="entry-title"> <?php echo get_the_title(); ?></h1>
            </header>
            
            <div class="sub-nav">
            	            <ul>
            	            <?php 
            		            $args = array(
            	                'authors'      => '',
            	                'child_of'     => 0,
            	                'depth'        => 0,
            	                'echo'         => 1,
            	                'exclude'      => '',
            	                'include'      => '',
            	                'post_type'    => 'titan_prod_specs',
            	                'post_status'  => 'publish',
            	                'sort_column'  => 'menu_order, post_title',
            	                'sort_order'   => '',
            	                'title_li'     => __(''),
            	                'walker'       => ''
            	            ); 
            	            ?>
            	<?php wp_list_pages( $args ); ?>
            	</ul>
            </div>
            	<span class="line-v"></span>
          </div>
		</div><!-- .col-sidebar -->

		<div class="col-sm-12 col-sm-8 col-md-9 col-lg-9">
            <div class="image">
	           <?php 
	        	 if( have_posts() ): while( have_posts() ): the_post();
	        	 	if( has_post_thumbnail()){
						echo aq_resizer_img('full', 800, 750);
					}
	        	 endwhile; endif;   
                ?>
           	</div>

		</div><!-- MAIN -->

<?php 
locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();