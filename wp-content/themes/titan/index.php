<?php get_header();
			
		get_template_part(HTML, 'before');
			if(have_posts()): while(have_posts()): the_post();
				echo '<article>';
				
						if ( has_post_thumbnail() ) {
						 	echo aq_resizer_img('full', 520, 240);
						 }
						get_template_part(PARTS, 'title' );
						get_template_part(PARTS, get_post_format() );
						 
				echo '</article>';
				
			endwhile; endif;
	
	get_template_part(HTML, 'after');

get_footer(); 

?>
