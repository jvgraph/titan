<?php 
	/*
		Template Name: resources
	*/
get_header();
			
		get_template_part(HTML, 'before');
		?>
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page">
            <div class="page-content">
					<h1 class="title"><?php echo "resources"; ?></h1>
					<div class="sub-nav">
                        <?php
					    wp_nav_menu( array(
					        'menu'              => 'cyty-material',
					        'theme_location'    => 'cyty-material',
					        'depth'             => 2,
					        'container'         => '',
					        'menu_class'        => 'cities-nav',
					        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					        'walker'            => new wp_bootstrap_navwalker())
					    );
					?>                       
					</div>
            </div>
 <span class="line-v"></span>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 material content-height">
			<div class="row">
				<ul id="people" class="clearfix">
				<?php
					$args = array(
					'post_type' => 'resources',
					'posts_per_page' => -1,				
					);
					$material = new WP_Query($args);
					
					
					
					if( $material->have_posts() ): while( $material->have_posts() ): $material->the_post(); 
					
					
					$filearray = get_post_meta( get_the_ID(), 'pdf_custom_attachment', true );
					$this_file = $filearray['url'];


					?>
					    <li <?php post_class("col-lg-3 col-md-3 col-sm-4 col-xs-6 person")?>>
							<div class="person-inner">
						<a href="<?php if($this_file != ""){echo $html .= $this_file;}?>" title="<?php the_title(); ?>">
						<?php
						if ( has_post_thumbnail() ) {
						 	echo aq_resizer_img('full', 360, 360);
						 }						
						?>
						<div class="person-info">
							<h3 class="name">
								<?php the_title();?>
								<span class="icon icon-download"></span>
							</h3>
						</div>
						</a>
						</div>
						<?php						
					echo '</li>';
				
					endwhile; endif;
				?>
			</ul>
		</div>
	</div>
	<?php
	get_template_part(HTML, 'after');

get_footer(); 

?>