<?php
/*
*
* Template Name: Home Page w/ Slider
*
*/
get_header();
	$args = array(
	'category_name'  => 'news',
	'posts_per_page' => 3
	);
	$home_slider = new WP_Query($args);
?>
<div class="container-fluid">

<div id="slider" class="full-slider">
	<div class="flexslider">
		<ul class="slides">
			
			<?php
			while($home_slider->have_posts()): $home_slider->the_post(); 
			
			
			
		 ?>
			<li <?php post_class(); ?>>
				<h2 class="tp-caption tp-resizeme">
					<a href="<?php the_permalink();?>" title="<?php the_title(); ?>">
						<?php 
					if( have_rows('slider_title') ){ 
					
					while( have_rows('slider_title') ): the_row(); 
					
					// vars
					$line_one = get_sub_field('first_line');
					$line_two = get_sub_field('second_line');
					$line_three = get_sub_field('third_line');
					
					
							echo $line_one . "<br/>" . $line_two . "<br/>" . $line_three; ?><span class="arrow"><svg version="1.1" id="Layer_1" x="0px" y="0px"
	 width="60px" height="60px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
<path fill="#FFFFFF" d="M30,55.5C15.9,55.5,4.5,44.1,4.5,30S15.9,4.5,30,4.5S55.5,15.9,55.5,30S44.1,55.5,30,55.5z M30,8.5
	C18.1,8.5,8.5,18.1,8.5,30S18.1,51.5,30,51.5c11.9,0,21.5-9.6,21.5-21.5S41.9,8.5,30,8.5z M32.3,42.2l-2.8-2.8l7.4-7.4H16.2v-4h20.7
	l-7.4-7.4l2.8-2.8L42.6,28h0.4v0.4l1.6,1.6l-1.6,1.6V32h-0.4L32.3,42.2z"/>
</svg></span>
<?php endwhile; }else{ ?>
<?php
			
							the_title(); ?><span class="arrow"><svg version="1.1" id="Layer_1" x="0px" y="0px"
	 width="60px" height="60px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
<path fill="#FFFFFF" d="M30,55.5C15.9,55.5,4.5,44.1,4.5,30S15.9,4.5,30,4.5S55.5,15.9,55.5,30S44.1,55.5,30,55.5z M30,8.5
	C18.1,8.5,8.5,18.1,8.5,30S18.1,51.5,30,51.5c11.9,0,21.5-9.6,21.5-21.5S41.9,8.5,30,8.5z M32.3,42.2l-2.8-2.8l7.4-7.4H16.2v-4h20.7
	l-7.4-7.4l2.8-2.8L42.6,28h0.4v0.4l1.6,1.6l-1.6,1.6V32h-0.4L32.3,42.2z"/>
</svg></span>
  <?php } ?>

					</a>
				</h2>
					<?php 
					get_template_part(PARTS, 'slider' ); 
					?>
			</li>
			<?php				
				endwhile;
			?>
		</ul>
	</div>
</div> <!-- slider -->
<!-- end of slider -->
<!-- ********************************* -->

<div id="tile-container" class="home-slider row clearfix">
		
			<?php
				$args = array(
					'post_type'       => 'page',
					'posts_per_page'  => 4,
					//'offset'          => 0,
					'meta_query'      => array( 
						array(
							'key' 	  => 'featured-checkbox',
							'value'   => 'yes'
						)
					)
				);
				$one = new WP_Query($args);
				
				while( $one->have_posts() ): $one->the_post();
			?>
			<div <?php post_class('tile col-lg-3 col-md-3 col-sm-3 col-xs-6'); ?>>
				<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 540, 459);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
			</div><!-- end .tile -->
			<?php endwhile; wp_reset_postdata(); ?>
	
</div><!-- end #tile-container -->


<!-- ********************************* -->
</div> <!-- container-fluid -->
<?php	
 get_footer(); ?>
