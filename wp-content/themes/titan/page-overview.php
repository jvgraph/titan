<?php 
/*
*
* Template Name: Overview Template
*
*/
get_header();
			
		get_template_part(HTML, 'before');
		?>
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page">
            <div class="page-content">
	<?php
		the_title('<h1 class="post-title">', '</h1>', true);
	?>
 <?php if( is_page('careers') ): ?>
    
<div class="opening-nav sub-nav">
	<ul>
		<li>
	  
	  
	      	<a href="http://hire.jobvite.com/CompanyJobs/Careers.aspx?k=JobListing&c=qFVaVfwp&v=1" style="font-size: 1.5em;" target="_blank">openings <span>
		      	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="20.9px" height="20.6px" viewBox="0 0 20.9 20.6" enable-background="new 0 0 20.9 20.6" xml:space="preserve">
<polyline fill="none" stroke="#CCCCCC" stroke-width="1.45" stroke-linecap="square" stroke-miterlimit="10" points="8.3,16.1 
	14.2,10.2 8.3,4.2 "/>
</svg>
</span></a>
	      	
	   </li>
	</ul>
</div>
<?php endif; ?>	
<div class="sub-nav">
<?php
if(!$post->post_parent){
	// will display the subpages of this top level page
	$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
}
else{

	if($post->ancestors) {
		// now you can get the the top ID of this page
		// wp is putting the ids DESC, thats why the top level ID is the last one
		$ancestors = get_post_ancestors($this_page);
		$children = wp_list_pages("title_li=&child_of=".$ancestors."&echo=0");
	}
}

if ($children) { ?>
	<ul>
		<?php echo $children; ?>
	</ul>
<?php } ?>

</div>
    </div>
 <span class="line-v"></span>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 staff content-height">
			<div class="our-philo">
				
			<div class="page-intro clearfix">
	                <div class="col-md-5 col-xs-12">
		                <h4 class="sub-title"><?php the_field( 'page_sub_title' ); ?></h4>
	                </div>
	                <div class="col-md-7 col-xs-12">
		                  <?php 
			                  if( have_posts() ): while( have_posts() ): the_post();
			                  the_content(); 
			                  endwhile; endif;
			                  ?>
	                </div>
                </div>
				
	                
	              <div class="img-large clearfix">
		              <?php if( get_field('gallery') ){ ?>
	
						<div class="entry flexslider">
							<?php
							
								?>
								<ul class="media-slides slides clearfix">
									<?php
									 while( has_sub_field('gallery') ):
									 
									 $thumb = get_sub_field('images');
									 $img_url = $thumb["url"];
									 $image = aq_resize( $img_url, 1100, 700, true );
									?>
									<li>
										
										<img src="<?php echo $image; ?>" />
									</li>
									<?php endwhile; ?>
								</ul>
						</div>
					<?php
						}else { 
	              		     echo aq_resizer_img('full', 1100, 700); 
	              		} 
	              	?>
	              </div>

		</div>
	</div>
	<?php
	get_template_part(HTML, 'after');

get_footer(); 

?>