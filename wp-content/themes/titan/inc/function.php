<?php

register_nav_menus( array(
	'company_subnav' => 'Company Subnav'
) );

//////////////////////////////////////////////


//////////////////////////////////////////////

/*-------------------------------------------------------------------------------------------*/
/* titan_prod_specs Post Type */
/*-------------------------------------------------------------------------------------------*/
class titan_prod_specs {

	function titan_prod_specs() {
		add_action('init',array($this,'create_post_type'));
	}

	function create_post_type() {
		$labels = array(
		    'name' => 'Production Specs',
		    'singular_name' => 'Spec',
		    'add_new' => 'Add New',
		    'all_items' => 'All Specs',
		    'add_new_item' => 'Add New Post',
		    'edit_item' => 'Edit Post',
		    'new_item' => 'New Post',
		    'view_item' => 'View Post',
		    'search_items' => 'Search Posts',
		    'not_found' =>  'No Posts found',
		    'not_found_in_trash' => 'No Posts found in trash',
		    'parent_item_colon' => 'Parent Post:',
		    'menu_name' => 'Specs'
		);
		$args = array(
			'labels' => $labels,
			'description' => "",
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'capability_type' => 'page',
			'hierarchical' => true,
			'supports' => array('title','editor', 'attachments','thumbnail','custom-fields','revisions','page-attributes'),
			'has_archive' => true,
			'rewrite' => array('slug' => 'production-specs'),
			'query_var' => true,
			'can_export' => true
		);
		register_post_type('titan_prod_specs',$args);
	}
}

$titan_prod_specs = new titan_prod_specs();

//////////////////////////////////////////////

add_filter( 'attachments_default_instance', '__return_false' ); // disable the default instance

function my_attachments( $attachments )
{
  $fields         = array(
    array(
      'name'      => 'title',                         // unique field name
      'type'      => 'text',                          // registered field type
      'label'     => __( 'Title', 'attachments' ),    // label to display
      'default'   => 'title',                         // default value upon selection
    )
  );

  $args = array(

    // title of the meta box (string)
    'label'         => 'Attachments',

    // all post types to utilize (string|array)
    'post_type'     => array( 'post', 'page', 'titan_prod_specs' ),

    // meta box position (string) (normal, side or advanced)
    'position'      => 'normal',

    // meta box priority (string) (high, default, low, core)
    'priority'      => 'high',

    // allowed file type(s) (array) (image|video|text|audio|application)
    'filetype'      => 'pdf',  // no filetype limit

    // include a note within the meta box (string)
    'note'          => 'Attach files here!',

    // by default new Attachments will be appended to the list
    // but you can have then prepend if you set this to false
    'append'        => true,

    // text for 'Attach' button in meta box (string)
    'button_text'   => __( 'Attach Files', 'attachments' ),

    // text for modal 'Attach' button (string)
    'modal_text'    => __( 'Attach', 'attachments' ),

    // which tab should be the default in the modal (string) (browse|upload)
    'router'        => 'browse',

    // whether Attachments should set 'Uploaded to' (if not already set)
    'post_parent'   => true,

    // fields array
    'fields'        => $fields,

  );

  $attachments->register( 'my_attachments', $args ); // unique instance name
}

add_action( 'attachments_register', 'my_attachments' );

function bd_parse_post_variables(){
// bd_parse_post_variables function for WordPress themes by Nick Van der Vreken.
// please refer to bydust.com/using-custom-fields-in-wordpress-to-attach-images-or-files-to-your-posts/
// for further information or questions.
global $post, $post_var;

// fill in all types you'd like to list in an array, and
// the label they should get if no label is defined.
// example: each file should get label "Download" if no
// label is set for that particular file.
$types = array('image' => 'no info available',
'file' => 'Download',
'link' => 'Read more...');

// this variable will contain all custom fields
$post_var = array();
foreach(get_post_custom($post->ID) as $k => $v) $post_var[$k] = array_shift($v);

// creating the arrays
foreach($types as $type => $message){
global ${'post_'.$type.'s'}, ${'post_'.$type.'s_label'};
$i = 1;
${'post_'.$type.'s'} = array();
${'post_'.$type.'s_label'} = array();
while($post_var[$type.$i]){
echo $type.$i.' - '.${$type.$i.'_label'};
array_push(${'post_'.$type.'s'}, $post_var[$type.$i]);
array_push(${'post_'.$type.'s_label'},  $post_var[$type.$i.'_label']?htmlspecialchars($post_var[$type.$i.'_label']):$message);
$i++;
}
}
}

//////////////////////////////////////////////

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'df_disable_comments_post_types_support');

// Close comments on the front-end
function df_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);

// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

// Remove comments page in menu
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');

// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');

// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'df_disable_comments_admin_bar');

//////////////////////////////////////////////

update_option('image_default_link_type','none');

update_option('image_default_align','none');

// add_th eme_support to resize thumbnails
add_theme_support( 'post-thumbnails' );

//add new image size for home slider
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'full-square', 1024, 1024, true );
	add_image_size( 'titan-thumb', 300, 300, true );
}

function my_custom_image_sizes($sizes) {
     // remove img sizes if needed
    unset( $sizes['slrg-thumb']);
   // add your image sizes, i.e.
/*
   $myCustomImgsizes = array(
        'full-width'=> __('Full Width')
   );
*/

   $newimgsizes = array_merge($sizes);
   return $newimgsizes;
}
add_filter('image_size_names_choose', 'my_custom_image_sizes');

//////////////////////////////////////////////

// Default page content loop
function titan_page() { ?>

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="entry-content">

    			<?php the_content(); ?>

            </div><!-- .entry-content -->

        </article><!-- #post-## -->
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>

<?php }

//////////////////////////////////////////////

function titan_people() { ?>

    <?php
        $postId = get_the_ID();

        query_posts(array(
            'post_parent' => $postId,
            'post_type' => 'page',
            'post-status' => 'published',
            'orderby'=>'post_date',
            'order'=> 'ASC',
            'posts_per_page' => -1,
        )  );
    ?>

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <li class="col-md-3 col-sm-3 col-xs-2">

            <?php if ( has_post_thumbnail() ) { the_post_thumbnail('titan-thumb'); } ?>

            <a class="overlay" href="<?php the_permalink() ?>" title="<?php printf( esc_attr__( '%s Permalink', 'boilerplate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
                <div>
                    <div class="inner">
                        <div class="entry-title">
                            <?php the_title(); ?>
                        </div>

                        <div class="entry-meta">
                            <?php echo get_post_meta( get_the_ID(), 'Job Title', true ); ?>
                        </div>
                    </div>
                </div>

            </a><!-- .overlay -->
        </li>
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>

<?php }

//////////////////////////////////////////////

// Post title
function titan_post_title($postId) { ?>

    <?php

        $postId = $postId;

        $args = array(
        	'child_of'     => $postId,
        	'post_status'  => 'publish',
        	'sort_column'  => 'menu_order',
        	'depth'        => 1,
        	'title_li'     => '',
        );

        wp_list_pages($args); ?>

<?php }

//////////////////////////////////////////////

// Post title
function titan_post_cat($post_cat) { ?>

    <?php
    $taxonomy     = 'category';
    $orderby      = 'name';
    $title        = '';
    $post_cat     = $post_cat;

    $args = array(
      'taxonomy'     => $taxonomy,
      'orderby'      => $orderby,
      'title_li'     => $title,
      'child_of'     => $post_cat
    );
    ?>

    <?php wp_list_categories( $args ); ?>

<?php }

//////////////////////////////////////////////

// Default page content loop
function titan_post_link() { ?>

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

        <li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
        </li><!-- #post-## -->

    <?php endwhile; ?>
    <?php wp_reset_query(); ?>

<?php }

//////////////////////////////////////////////

?>