<?php
///asyn script
function add_async_forscript($url)
{
    if (strpos($url, '#asyncload')===false)
        return $url;
    else if (is_admin())
        return str_replace('#asyncload', '', $url);
    else
        return str_replace('#asyncload', '', $url)."' async='async"; 
}
add_filter('clean_url', 'add_async_forscript', 11, 1);
/// load scripts
function titan_scripts() {
	
	if( is_admin() ) {
		wp_enqueue_style( 'titan-admin', get_template_directory_uri().'/css/admin.css', array(), '08192014', 'all' );
		wp_enqueue_script('admin-js', get_template_directory_uri().'/js/admin.js', array(), false, true);
	}
	
	if (!is_admin()) {	
		wp_enqueue_style( 'titan-style', get_template_directory_uri() . '/css/style.css', array(), '07312014', 'all' );	
	
	
	wp_deregister_script('jquery'); 
	//wp_enqueue_script('jquery');
	wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', false, '1.11.0', true);
	//wp_enqueue_script('modernzr', '//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.7.2.js', false, '2.6.0', true);
	wp_enqueue_script('js-ui', '//code.jquery.com/ui/1.11.2/jquery-ui.min.js', array('jquery'), false, true);

	wp_enqueue_script('app', get_template_directory_uri().'/js/app.min.js', array('jquery'), false, true);
	}
}
add_action( 'wp_enqueue_scripts', 'titan_scripts' );

///remove query string

function remove_cssjs_ver( $src ) {
 if( strpos( $src, '?ver=' ) )
 $src = remove_query_arg( 'ver', $src );
 return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );