<?php

/**
 * Adds a meta box to the post editing screen
 */
function titan_featured_meta() {
    add_meta_box( 'titan_meta', __( 'Featured Page', 'titan' ), 'titan_meta_callback', 'page', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'titan_featured_meta' );

/**
 * Outputs the content of the meta box
 */

function titan_meta_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'titan_nonce' );
    $titan_stored_meta = get_post_meta( $post->ID );
    ?>

 <p>
    <span class="titan-row-title"><?php _e( 'Check the box to have the page appear in the home page supergrid menu: ', 'titan' )?></span>
    <div class="titan-row-content">
        <label for="featured-checkbox">
            <input type="checkbox" name="featured-checkbox" id="featured-checkbox" value="yes" <?php if ( isset ( $titan_stored_meta['featured-checkbox'] ) ) checked( $titan_stored_meta['featured-checkbox'][0], 'yes' ); ?> />
            <?php _e( 'Featured Page', 'titan' )?>
        </label>

    </div>
</p>   

    <?php
}

/**
 * Saves the custom meta input
 */
function titan_meta_save( $post_id ) {

    // Checks save status - overcome autosave, etc.
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'titan_nonce' ] ) && wp_verify_nonce( $_POST[ 'titan_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

// Checks for input and saves - save checked as yes and unchecked at no
if( isset( $_POST[ 'featured-checkbox' ] ) ) {
    update_post_meta( $post_id, 'featured-checkbox', 'yes' );
} else {
    update_post_meta( $post_id, 'featured-checkbox', 'no' );
}

}
add_action( 'save_post', 'titan_meta_save' );
/******************************************************************************************************************** 
*
*@param !people title!
*
*********************************************************************************************************************/
add_action("add_meta_boxes", "title_add_meta_box");

function title_add_meta_box(){

	add_meta_box(
		"person_header",
		"Person Title",
		"person_title_meta_box",
		"people",
		"normal" 
	);
}
function person_title_meta_box($post){
$person_headline = get_post_meta($post->ID, 'person_headline', true);
	wp_nonce_field( 'person_meta_nonce', 'person_nonce' ); ?>
	
	<p>
		<input class="widefat" name="person_headline" id="person_headline" value="<?php echo $person_headline; ?>">		
	</p>
	<?php
}


add_action('save_post', 'custom_save_person_headline');
function custom_save_person_headline($post_id){

	//if(define('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
	
	
	if(!isset($_POST['person_nonce']) || !wp_verify_nonce($_POST['person_nonce'], 'person_meta_nonce')) return;
	
	// save or update
	
	if(isset($_POST['person_headline']) && $_POST['person_headline'] != ''){
		update_post_meta($post_id, 'person_headline', esc_html($_POST['person_headline']));
	}
}
/******************************************************************************************************************** 
*
* address––office
*
*********************************************************************************************************************/
add_action("add_meta_boxes", "address_add_meta_box");

function address_add_meta_box(){

	add_meta_box(
		"office_address",
		"Office Address",
		"office_address_meta_box",
		"office",
		"normal" 
	);
}
function office_address_meta_box($post){
$office_location = get_post_meta($post->ID, 'office_location', true);
	wp_nonce_field( 'address_meta_nonce', 'address_nonce' ); ?>
	
	<p>
		<input class="widefat" name="office_location" id="office_location" value="<?php echo esc_html($office_location); ?>" placeholder="please insert address here">		
	</p>
	<?php
}


add_action('save_post', 'custom_save_office_location');
function custom_save_office_location($post_id){

	//if(define('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
	
	
	if(!isset($_POST['address_nonce']) || !wp_verify_nonce($_POST['address_nonce'], 'address_meta_nonce')) return;
	
	// save or update
	
	if(isset($_POST['office_location']) && $_POST['office_location'] != ''){
		update_post_meta($post_id, 'office_location', esc_html($_POST['office_location']));
	}
}
/******************************************************************************************************************** 
*
* phone number
*
*********************************************************************************************************************/

add_action("add_meta_boxes", "phone_add_meta_box");

function phone_add_meta_box(){

	add_meta_box(
		"office_phone",
		"Office phone",
		"office_phone_meta_box",
		"office",
		"normal" 
	);
}
function office_phone_meta_box($post){
$office_phone_num = get_post_meta($post->ID, 'office_phone_num', true);
	wp_nonce_field( 'phone_meta_nonce', 'phone_nonce' ); ?>
	
	<p>
		<input type="text" class="widefat" name="office_phone_num" id="office_phone_num" value="<?php echo esc_html($office_phone_num); ?>" placeholder="please insert phone here">		
	</p>
	<?php
}


add_action('save_post', 'custom_save_office_phone_num');
function custom_save_office_phone_num($post_id){

	//if(define('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
	
	
	if(!isset($_POST['phone_nonce']) || !wp_verify_nonce($_POST['phone_nonce'], 'phone_meta_nonce')) return;
	
	// save or update
	
	if(isset($_POST['office_phone_num']) && $_POST['office_phone_num'] != ''){
		update_post_meta($post_id, 'office_phone_num', esc_html($_POST['office_phone_num']));
	}
}

/******************************************************************************************************************** 
*
* fax number
*
*********************************************************************************************************************/
add_action("add_meta_boxes", "fax_add_meta_box");

function fax_add_meta_box(){

	add_meta_box(
		"office_fax",
		"Office fax",
		"office_fax_meta_box",
		"office",
		"normal" 
	);
}
function office_fax_meta_box($post){
$office_fax_num = get_post_meta($post->ID, 'office_fax_num', true);
	wp_nonce_field( 'fax_meta_nonce', 'fax_nonce' ); ?>
	
	<p>
		<input type="text" class="widefat" name="office_fax_num" id="office_fax_num" value="<?php echo esc_html($office_fax_num); ?>" placeholder="please insert fax here">		
	</p>
	<?php
}


add_action('save_post', 'custom_save_office_fax_num');
function custom_save_office_fax_num($post_id){

	//if(define('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
	
	
	if(!isset($_POST['fax_nonce']) || !wp_verify_nonce($_POST['fax_nonce'], 'fax_meta_nonce')) return;
	
	// save or update
	
	if(isset($_POST['office_fax_num']) && $_POST['office_fax_num'] != ''){
		update_post_meta($post_id, 'office_fax_num', esc_html($_POST['office_fax_num']));
	}
}
/******************************************************************************************************************** 
*
* Contact email at the Office
*
*********************************************************************************************************************/
add_action("add_meta_boxes", "email_add_meta_box");

function email_add_meta_box(){

	add_meta_box(
		"office_email",
		"Office email",
		"office_email_meta_box",
		"office",
		"normal" 
	);
}
function office_email_meta_box($post){
$office_email = get_post_meta($post->ID, 'office_email', true);
	wp_nonce_field( 'email_meta_nonce', 'email_nonce' ); ?>
	
	<p>
		<input class="widefat" type="email" name="office_email" id="office_email" value="<?php echo esc_html($office_email); ?>" placeholder="please insert email here">		
	</p>
	<?php
}


add_action('save_post', 'custom_save_office_email');
function custom_save_office_email($post_id){

	//if(define('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
	
	if(!isset($_POST['email_nonce']) || !wp_verify_nonce($_POST['email_nonce'], 'email_meta_nonce')) return;
	
	// save or update
	
	if(isset($_POST['office_email']) && $_POST['office_email'] != ''){
		update_post_meta($post_id, 'office_email', esc_html($_POST['office_email']));
	}
}
/******************************************************************************************************************** 
*
*	PDF METABOX  
*
*********************************************************************************************************************/

/**
 * Register the 'Add PDF' meta box for the 'Material' post type.
 *
 * @since    1.0.0
 */

/////////////// add PDF file to material posts
//////////////////////////////////////////////
/////////////////////////////////////////////
function add_custom_meta_boxes() {  
    add_meta_box('pdf_custom_attachment', 'Material PDF', 'pdf_custom_attachment', 'resources', 'side', 'core');  
}
add_action('add_meta_boxes', 'add_custom_meta_boxes');  

function pdf_custom_attachment() {  
    wp_nonce_field(plugin_basename(__FILE__), 'pdf_custom_attachment_nonce');
    $html = '<p class="description">';
    $html .= 'Upload your PDF here.';
    $html .= '</p>';
    $html .= '<input type="file" id="pdf_custom_attachment" name="pdf_custom_attachment" value="" size="25">';
    echo $html;
}

add_action('save_post', 'save_custom_meta_data');
function save_custom_meta_data($id) {
    if(!empty($_FILES['pdf_custom_attachment']['name'])) {
        $supported_types = array('application/pdf');
        $arr_file_type = wp_check_filetype(basename($_FILES['pdf_custom_attachment']['name']));
        $uploaded_type = $arr_file_type['type'];

        if(in_array($uploaded_type, $supported_types)) {
            $upload = wp_upload_bits($_FILES['pdf_custom_attachment']['name'], null, file_get_contents($_FILES['pdf_custom_attachment']['tmp_name']));
            if(isset($upload['error']) && $upload['error'] != 0) {
                wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
            } else {
                update_post_meta($id, 'pdf_custom_attachment', $upload);
            }
        }
        else {
            wp_die("The file type that you've uploaded is not a PDF.");
        }
    }
}

function update_edit_form() {
    echo ' enctype="multipart/form-data"';
}
add_action('post_edit_form_tag', 'update_edit_form');