<?php

function aq_resizer_img($thumb_id, $width, $height){
	$thumb = get_post_thumbnail_id(); 
	$img_url = wp_get_attachment_url( $thumb,"$thumb_id" ); 
	$image = aq_resize( $img_url, $width, $height, true );
	 ?>
   <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" class="img-responsive" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
	<?php 
}