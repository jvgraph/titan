<?php
	
add_action( 'init', 'city_tax' );

function city_tax() {
		$labels = array(
		'name'              => _x( 'Markets', 'taxonomy general name' ),
		'singular_name'     => _x( 'Market', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Markets' ),
		'all_items'         => __( 'All Markets' ),
		'parent_item'       => __( 'Parent Market' ),
		'parent_item_colon' => __( 'Parent Market:' ),
		'edit_item'         => __( 'Edit Market' ),
		'update_item'       => __( 'Update Market' ),
		'add_new_item'      => __( 'Add New Market' ),
		'new_item_name'     => __( 'New Market Name' ),
		'menu_name'         => __( 'Market' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'market' ),
	);
	register_taxonomy( 'market', array( 'people' ), $args );
}
//////////materials tax
add_action( 'init', 'material_tax' );
function material_tax() {
		$labels = array(
		'name'              => _x( 'Materials', 'taxonomy general name' ),
		'singular_name'     => _x( 'Material', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Materials' ),
		'all_items'         => __( 'All Materiales' ),
		'parent_item'       => __( 'Parent Material' ),
		'parent_item_colon' => __( 'Parent Material:' ),
		'edit_item'         => __( 'Edit Material' ),
		'update_item'       => __( 'Update Material' ),
		'add_new_item'      => __( 'Add New Material' ),
		'new_item_name'     => __( 'New Material Name' ),
		'menu_name'         => __( 'Material' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'material' ),
	);
	register_taxonomy( 'material', array( 'resources' ), $args );
}
//////////materials tax
add_action( 'init', 'video_tax' );
function video_tax() {
		$labels = array(
		'name'              => _x( 'Videos', 'taxonomy general name' ),
		'singular_name'     => _x( 'Video', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Videos' ),
		'all_items'         => __( 'All Videos' ),
		'parent_item'       => __( 'Parent Video' ),
		'parent_item_colon' => __( 'Parent Video:' ),
		'edit_item'         => __( 'Edit Video' ),
		'update_item'       => __( 'Update Video' ),
		'add_new_item'      => __( 'Add New Video' ),
		'new_item_name'     => __( 'New Video Name' ),
		'menu_name'         => __( 'Video' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'video' ),
	);
	register_taxonomy( 'video', array( 'resources' ), $args );
}
