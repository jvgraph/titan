<?php 
/*
*
* Template Name: People by city
*
*/
get_header();
			
		get_template_part(HTML, 'before');
		?>
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page">
    <div class="page-content">
		<h1 class="post-title"><?php echo get_the_title(); ?> team</h1>
        
    </div>
 <span class="line-v"></span>
</div>
		
		
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 staff content-height">
			<div class="row">
				
				<?php if( have_rows('people') ): ?>
				
				<ul id="people" class="clearfix">
				
				<?php while( have_rows('people') ): the_row();
					
					$name = get_sub_field('name');
					$position = get_sub_field('position');
					$image = get_sub_field('person_image');
					 ?>
				
					    <li <?php post_class("col-lg-3 col-md-3 col-sm-4 col-xs-6 person")?>>
							<div class="person-inner">
						
						<a href="#"><?php
							$params = array( 'width' => 360, 'height' => 370, 'crop' => true );
							$image = bfi_thumb( $image['url'], $params );
							echo "<img src='$image'/>";	
												
						?>
						<div class="person-info">
							<h3 class="name">
								<?php echo $name; ?>
								<span class="position">
								<?php echo $position; ?>
								</span>
							</h3>
						</div>
						
						</div></a>
												
					</li>
					<?php endwhile; ?>
			</ul>
			<?php endif; ?>
		</div>
	</div>
	<?php
	get_template_part(HTML, 'after');

get_footer(); 

?>