<?php
/*
*
* Template Name: Home Page Smart Grid
*
*/
get_header(); 

get_template_part(HTML, 'before');
?>	

<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 smart-grid">
	<div class="row">
		<?php 
			$args = array(
			'post_type' => array('page', 'post'),
			'tag__in'  => 31, 'posts_per_page' => 3
			);
			$slider_top = new WP_Query($args);	
		?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tile">
			<div class="flexslider">
			<ul class="slides">
			<?php while( $slider_top->have_posts() ): $slider_top->the_post(); ?>
			<li><h2> 
					<a href="<?php the_permalink();?>" title="<?php the_title(); ?>">
						<?php 
					if( have_rows('slider_title') ){ 
					
					while( have_rows('slider_title') ): the_row(); 
					
					// vars
					$line_one = get_sub_field('first_line');
					$line_two = get_sub_field('second_line');
					$line_three = get_sub_field('third_line');
					
					
							echo $line_one . "<br/>" . $line_two . "<br/>" . $line_three; ?><span class="arrow"><svg version="1.1" id="arrow" x="0px" y="0px"
									 width="75px" height="75px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
							<path fill="#FFFFFF" d="M30,55.5C15.9,55.5,4.5,44.1,4.5,30S15.9,4.5,30,4.5S55.5,15.9,55.5,30S44.1,55.5,30,55.5z M30,8.5
									C18.1,8.5,8.5,18.1,8.5,30S18.1,51.5,30,51.5c11.9,0,21.5-9.6,21.5-21.5S41.9,8.5,30,8.5z M32.3,42.2l-2.8-2.8l7.4-7.4H16.2v-4h20.7
									l-7.4-7.4l2.8-2.8L42.6,28h0.4v0.4l1.6,1.6l-1.6,1.6V32h-0.4L32.3,42.2z"/>
							</svg></span>
							<?php endwhile; }else{ ?>
							<?php
							
							the_title(); ?><span class="arrow"><svg version="1.1" id="arraow_1" x="0px" y="0px"
									 width="75px" height="75px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
							<path fill="#FFFFFF" d="M30,55.5C15.9,55.5,4.5,44.1,4.5,30S15.9,4.5,30,4.5S55.5,15.9,55.5,30S44.1,55.5,30,55.5z M30,8.5
									C18.1,8.5,8.5,18.1,8.5,30S18.1,51.5,30,51.5c11.9,0,21.5-9.6,21.5-21.5S41.9,8.5,30,8.5z M32.3,42.2l-2.8-2.8l7.4-7.4H16.2v-4h20.7
									l-7.4-7.4l2.8-2.8L42.6,28h0.4v0.4l1.6,1.6l-1.6,1.6V32h-0.4L32.3,42.2z"/>
							</svg></span>
							  <?php } ?>
							
					</a>
									</h2>
					<?php 
						if ( has_post_thumbnail() ) {
							echo aq_resizer_img('full', 625, 540);
						} 
						 
					?>
					</li>
			<?php endwhile; 
				wp_reset_postdata();
			?>
			</ul>
			</div> <!-- slider -->
			
		</div> <!-- col -->
		
		<!-- ********************************************************** -->
			<?php
				$args = array(
					'post_type'       => 'page',
					'posts_per_page'  => 1,
					'offset'          => 0,
					'meta_query'      => array( 
						array(
							'key' 	  => 'featured-checkbox',
							'value'   => 'yes'
						)
					)
				);
				$one = new WP_Query($args);
				while( $one->have_posts() ): $one->the_post();
			?>
			<div <?php post_class('tile col-lg-12 col-md-12 col-sm-12 col-xs-12');?> id="id-<?php the_id(); ?>">
				<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 625, 342);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
			</div><!-- end .tile -->
			<?php endwhile; wp_reset_postdata(); ?>
	<!-- ********************************************************** -->
			
	<!-- ********************************************************** -->
			
	</div> <!-- row -->
	
</div> <!-- end of first col-left -->

<div class="second-col col-xs-12 col-sm-6 col-md-7 col-lg-7 smart-grid">
	<div class="row">
	<?php
		$argsd = array(
			'post_type'       => 'page',
			'posts_per_page'  => 1,
			'offset'          => 3,
			'meta_query'      => array( 
					array(
						'key' 	=> 'featured-checkbox',
						'value' => 'yes'
					)
				)
		);
		$four = new WP_Query($argsd);
		while( $four->have_posts() ): $four->the_post();
	?>
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 tile">
		<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">					
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 450, 390);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
	</div>
	<?php endwhile; wp_reset_postdata(); ?>
	<div id="twitter" class="col-xs-12 col-sm-12 col-md-8 col-lg-8 widget-area tile" role="complementary">
		<?php if ( is_active_sidebar( 'twitter' ) ) : ?>
		<div class="social"><?php dynamic_sidebar( 'twitter' ); ?></div>
		<?php endif; ?>
	</div><!-- #twitter -->
	
	<?php
		$argsg = array(
			'post_type'       => 'page',
			'posts_per_page'  => 1,
			'offset'          => 4,
			'meta_query'      => array( 
					array(
						'key' 	=> 'featured-checkbox',
						'value' => 'yes'
					)
				)
		);
		$five = new WP_Query($argsg);
		while( $five->have_posts() ): $five->the_post();
	?>
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 tile">
		<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">					
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 750, 710);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
	</div>
	<?php endwhile; wp_reset_postdata(); ?>
	
	
	
	</div> <!-- end row -->
</div> <!-- end second col -->
<div id="bottom-row" class="clearfix row">
	<?php
				$argsm = array(
					'post_type'       => 'page',
					'posts_per_page'  => 2,
					'offset'          => 1,
					'meta_query'      => array( 
							array(
								'key' 	=> 'featured-checkbox',
								'value' => 'yes'
							)
						)
				);
				$three = new WP_Query($argsm);
				while( $three->have_posts() ): $three->the_post();
			?>
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
			<div class="row">
				<div <?php post_class('tile col-lg-6 col-md-6 col-sm-6 col-xs-6');?> id="id-<?php the_id(); ?>">
					<a href="<?php the_permalink(); ?>" class="tile-link">
						<div class="overlay-container">				
							<h3 class="tile-title"><?php the_title(); ?></h3>						
							<div class="tile-overlay">					
							</div><!-- end .tile-overlay -->
							<?php				
							if ( has_post_thumbnail() ) {
							 	aq_resizer_img('full', 500, 420);
							 }
						 ?>
						</div><!-- end .overlay-container -->
					</a>
				</div>
<!-- 				•••••••••••••••••••••••••••••• -->

			</div>
			</div><!-- end .tile -->
			<?php endwhile; wp_reset_postdata(); ?>
			
			<?php
		$argsk = array(
			'post_type'       => 'page',
			'posts_per_page'  => 1,
			'offset'          => 5,
			'meta_query'      => array( 
					array(
						'key' 	=> 'featured-checkbox',
						'value' => 'yes'
					)
				)
		);
		$six = new WP_Query($argsk);
		while( $six->have_posts() ): $six->the_post();
	?>
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 tile">
		<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">					
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 590, 305);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
	</div>
	<?php endwhile; wp_reset_postdata(); ?>
	
</div>

<?php 
get_template_part(HTML, 'after');
get_footer(); ?>