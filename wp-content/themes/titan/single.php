<?php

get_header();
	
	get_template_part(HTML, 'before');

	if(have_posts()): while(have_posts()): the_post();
	
		if( is_singular(27) ){
			
			include('parts/content-news.php');
			
		}else{
			
			get_template_part(PARTS, get_post_format());
		}
		
	endwhile; endif;
	
	get_template_part(HTML, 'after');
	
get_footer();