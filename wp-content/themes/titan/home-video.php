<?php
/*
*
* Template Name: Home Page w/ Video
*
*/
get_header(); 

get_template_part(HTML, 'before');
?>
	
	<div id="tile-container" class="home-video clearfix">
		<div id="left-container" class="col-xs-12 col-sm-12 col-md-8 col-lg-8 clearfix">
		
		<div class="row">
			<?php 
				$args = array(
				'post_type' => array('page', 'post'),
				'tag__in'  => 31,
				'posts_per_page' => 3
				);
					$home_slider = new WP_Query($args);
					
				?>			
			<div <?php post_class('tile video-post col-md-12');?> id="id-<?php the_id(); ?>">
				<div class="flexslider">
				<ul class="slides">
				  <?php while ( $home_slider->have_posts() ): 
					$home_slider->the_post(); ?>
				
			
					<li><h2> 
					<a href="<?php the_permalink();?>" title="<?php the_title(); ?>">
						<?php 
					if( have_rows('slider_title') ){ 
					
					while( have_rows('slider_title') ): the_row(); 
					
					// vars
					$line_one = get_sub_field('first_line');
					$line_two = get_sub_field('second_line');
					$line_three = get_sub_field('third_line');
					
					
							echo $line_one . "<br/>" . $line_two . "<br/>" . $line_three; ?><span class="arrow"><svg version="1.1" id="arr" x="0px" y="0px"
									 width="75px" height="75px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
							<path fill="#FFFFFF" d="M30,55.5C15.9,55.5,4.5,44.1,4.5,30S15.9,4.5,30,4.5S55.5,15.9,55.5,30S44.1,55.5,30,55.5z M30,8.5
									C18.1,8.5,8.5,18.1,8.5,30S18.1,51.5,30,51.5c11.9,0,21.5-9.6,21.5-21.5S41.9,8.5,30,8.5z M32.3,42.2l-2.8-2.8l7.4-7.4H16.2v-4h20.7
									l-7.4-7.4l2.8-2.8L42.6,28h0.4v0.4l1.6,1.6l-1.6,1.6V32h-0.4L32.3,42.2z"/>
							</svg></span>
							<?php endwhile; }else{ ?>
							<?php
							
							the_title(); ?><span class="arrow"><svg version="1.1" id="arrow" x="0px" y="0px"
									 width="75px" height="75px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
							<path fill="#FFFFFF" d="M30,55.5C15.9,55.5,4.5,44.1,4.5,30S15.9,4.5,30,4.5S55.5,15.9,55.5,30S44.1,55.5,30,55.5z M30,8.5
									C18.1,8.5,8.5,18.1,8.5,30S18.1,51.5,30,51.5c11.9,0,21.5-9.6,21.5-21.5S41.9,8.5,30,8.5z M32.3,42.2l-2.8-2.8l7.4-7.4H16.2v-4h20.7
									l-7.4-7.4l2.8-2.8L42.6,28h0.4v0.4l1.6,1.6l-1.6,1.6V32h-0.4L32.3,42.2z"/>
							</svg></span>
							  <?php } ?>
							
					</a>
									</h2>
					<?php 
						if ( has_post_thumbnail() ) {
							echo aq_resizer_img('full', 940, 700);
						} 
						 
					?>
					</li>
					  <?php endwhile; ?>
					
					</ul>
			
			
			</div>
							
					
	
			</div><!-- end #video-tile -->	
			<?php 
			
			wp_reset_query();
			?>
	<!-- ********************************************************** -->
			<?php
				$args = array(
					'post_type'       => 'page',
					'posts_per_page'  => 1,
					'offset'          => 0,
					'meta_query'      => array( 
						array(
							'key' 	  => 'featured-checkbox',
							'value'   => 'yes'
						)
					)
				);
				$one = new WP_Query($args);
				
				while( $one->have_posts() ): $one->the_post();
			?>
			<div <?php post_class('tile col-lg-4 col-md-4 col-sm-4 col-xs-6');?> id="id-<?php the_id(); ?>">
				<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 500, 420);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
			</div><!-- end .tile -->
			<?php endwhile; wp_reset_postdata(); ?>
	<!-- ********************************************************** -->
			<?php
				$argsm = array(
					'post_type'       => 'page',
					'posts_per_page'  => 1,
					'offset'          => 1,
					'meta_query'      => array( 
							array(
								'key' 	=> 'featured-checkbox',
								'value' => 'yes'
							)
						)
				);
				$three = new WP_Query($argsm);
				while( $three->have_posts() ): $three->the_post();
			?>
			<div <?php post_class('tile col-lg-4 col-md-4 col-sm-4 col-xs-6');?> id="id-<?php the_id(); ?>">
				<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">					
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 500, 420);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
			</div><!-- end .tile -->
			<?php endwhile; wp_reset_postdata(); ?>
	<!-- ********************************************************** -->
			<?php
				$argst = array(
					'post_type'       => 'page',
					'posts_per_page'  => 1,
					'offset'          => 2,
					'meta_query'      => array( 
							array(
								'key' 	=> 'featured-checkbox',
								'value' => 'yes'
							)
						)
				);
				$four = new WP_Query($argst);
				
				while( $four->have_posts() ): $four->the_post();
			?>
			<div <?php post_class('print-tile tile col-lg-4 col-md-4 col-sm-4 col-xs-12'); ?> id="id-<?php the_id(); ?>">
				<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">					
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 500, 420);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
			</div><!-- end .print-tile -->			
			<?php endwhile; wp_reset_postdata(); ?>
	<!-- ********************************************************** -->
			</div><!-- end .row -->
		</div><!-- end #left-container -->
	<!-- ********************************************************** -->
	<!-- ********************************************************** -->
	<!-- ********************************************************** -->
		<div id="right-container" class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="row">		
		
			<?php
				$args1 = array(
					'post_type'       => 'page',
					'posts_per_page'  => 1,
					'offset'          => 3,
					'meta_query'      => array( 
							array(
								'key' 	=> 'featured-checkbox',
								'value' => 'yes'
							)
						)
				);
				$two = new WP_Query($args1);
				
				while( $two->have_posts() ): $two->the_post();
			?>

			<div <?php post_class('chat-tile tile col-md-12 col-sm-6 col-xs-6'); ?> id="id-<?php the_id(); ?>">
				<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">					
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 400, 230);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
			</div><!-- end .chat-tile -->
			<?php endwhile; wp_reset_postdata(); ?>
	<!-- ********************************************************** -->
			<?php
				$argsc = array(
					'post_type'       => 'page',
					'posts_per_page'  => 1,
					'offset'          => 4,
					'meta_query'      => array( 
							array(
								'key' 	=> 'featured-checkbox',
								'value' => 'yes'
							)
						)
				);
				$contact = new WP_Query($argsc);
				
				while( $contact->have_posts() ): $contact->the_post();
			?>
			
			<div <?php post_class('contact-tile tile col-md-12 col-sm-6 col-xs-6'); ?> id="id-<?php the_id(); ?>">
				<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">				
						<h3 class="tile-title"><?php the_title(); ?></h3>						
						<div class="tile-overlay">					
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 400, 230);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
			</div><!-- end .contact-tile -->
			<?php endwhile; wp_reset_postdata(); ?>
	<!-- ********************************************************** -->
			<?php
				$argsc = array(
					'post_type'       => 'page',
					'posts_per_page'  => 1,
					'offset'          => 5,
					'meta_query'      => array( 
							array(
								'key' 	=> 'featured-checkbox',
								'value' => 'yes'
							)
						)
				);
				$contact = new WP_Query($argsc);
				
				while( $contact->have_posts() ): $contact->the_post();
			?>
			
			<div <?php post_class('tile col-md-12 col-sm-12 col-xs-12'); ?> id="id-<?php the_id(); ?>">
				<a href="<?php the_permalink(); ?>" class="tile-link">
					<div class="overlay-container">	
						<?php if(!is_page(30) ){ ?>			
						<h3 class="tile-title"><?php the_title(); ?></h3>	
						<?php }?>					
						<div class="tile-overlay">					
						</div><!-- end .tile-overlay -->
						<?php				
						if ( has_post_thumbnail() ) {
						 	aq_resizer_img('full', 800, 710);
						 }
					 ?>
					</div><!-- end .overlay-container -->
				</a>
			</div><!-- end .tile -->
			<?php endwhile; wp_reset_postdata(); ?>
	<!-- ********************************************************** -->
		</div><!-- end .row -->
			
		</div><!-- end #right-container -->
	
	</div><!-- end #tile-container -->
	
<?php 
get_template_part(HTML, 'after');
get_footer(); ?>