<?php
/*
*
* Template Name: Contact Page
*
*/
get_header();

	locate_template(array('html-parts/html-before.php'), true, true);
	?>
	
<?php	
	if(have_posts()): while(have_posts()): the_post();
	?>
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page">
			<div class="col-sidebar page-content">
				<header class="title"><h1><?php the_title(); ?></h1></header>
				<?php if( is_page('contact') ): ?>
				<div class="sub-nav">
					<?php
					    wp_nav_menu( array(
					        'menu'              => 'ask-titan',
					        'theme_location'    => 'ask-titan',
					        'depth'             => 2,
					        'container'         => '',
					        'menu_class'        => 'ask-titan-nav',
					        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					        'walker'            => new wp_bootstrap_navwalker())
					    );
					?>
				</div>
				<?php endif; 
					if( get_field( "overview" ) ):
				?>
				<div class="entry tab accordion">
			        
		            <h2 class='collapser'>
			            overview
			        </h2>
		            
			        <div class="entry-content">
				        <div class="bio collapse-content">
			        		<?php 
				        		the_field("overview"); 
				        	?>
			        	</div>
			        </div>
			        
		        </div>
		        <?php endif; ?>
			</div>
			<span class="line-v"></span>
		</div>
		
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 content-height">
			
			<div class="entry offices">
				<?php
					if( !is_page('contact') ): 
						the_content(); 
					else:
						aq_resizer_img('full', 980, 580);
					endif;
					?>	
			</div>
			
		</div>
	<?php	
	endwhile; endif;
	
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();