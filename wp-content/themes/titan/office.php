<?php
/*
*
* Template Name: Offices PAGE
*
*/
get_header();

	locate_template(array('html-parts/html-before.php'), true, true);
	?>
	<div class="col-md-3 col-sm-3 col-xs-12 page">
			<?php 
					
				$offices_page = new WP_Query("page_id=1531");
				
				while($offices_page->have_posts()): $offices_page->the_post();
				
				$content = get_the_content();
				 ?>
				
			<div class="page-content">
				<h1 class="post-title">
			<?php
				the_title();
			?>
			</h1>
			<?php if( $content != "" ){ ?>
		        <div class="entry tab accordion">
			        
		            <h2 class='collapser'>
			            overview
			        </h2>
		            
			        <div class="entry-content">
				        <div class="bio collapse-content">
			        		<?php 
				        		the_content(); 
				        	?>
			        	</div>
			        </div>
			        
		        </div>
		        <?php } ?>
		    </div>
			<span class="line-v"></span>
			
		</div> <!-- page -->
		
		
		<div class="col-md-9 col-sm-9 col-xs-12 content-height">
<?php	
		
		if( have_rows('offices') ):
		while( have_rows('offices') ): the_row();
		///var
		$city = get_sub_field('city');
		$address = get_sub_field('address');
		$phone = get_sub_field('phone');
		$fax = get_sub_field('fax');
		$email = get_sub_field('email');
	?>
			
			<div class="entry offices office-wrap">
					<ul class="row">
						<li class="city col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<h3><span class="line"><?php echo $city; ?></span></h3>
					</li>
						<li class="address col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<p><?php 
							if( $address != ''){
								echo $address;
							}else{
								echo "please contact our {$address} office";
							}
						?></p>
						</li>
						<li class="phone-num col-xs-5 col-sm-4 col-sm-3 col-lg-3">
						<span class="icon-phone"></span>
						<?php 
							if($phone != ''){
								echo $phone;
							}else{
								echo '- -';
							}
						?>
						</li>
						<li class="fax-num col-xs-5 col-sm-4 col-md-3 col-lg-3" data-icon="l">
						<?php 
							if($fax != ''){
								echo $fax;
							}else{
								echo '- -';
							}
						?>
						</li>
						<li class="contact col-xs-2 col-sm-4 col-md-1 col-lg-1">
							<a href="<?php 
								if($email != ''){
									echo $email;
								}		
									?>">
								<span class="icon-contact-flash"></span>
							</a>
						</li>
					</ul>
			</div>
			
		
	<?php
		endwhile;
		endif;	
	endwhile;
	
	?>
	</div>
	<?php
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();