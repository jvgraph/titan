<?php
/*
*
* Template Name: Media Page
*
*/
get_header();

	locate_template(array('html-parts/html-before.php'), true, true);
	?>
	<div class="col-md-3 col-sm-3 col-xs-12 page">
			
			<div class="page-content">
				
				<header class="title"><h1><?php echo get_the_title(); ?></h1></header>
				<div class="sub-nav">
					<?php
					    wp_nav_menu( array(
					        'menu'              => 'media',
					        'theme_location'    => 'media',
					        'depth'             => 2,
					        'container'         => '',
					        'menu_class'        => 'media-nav',
					        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					        'walker'            => new wp_bootstrap_navwalker())
					    );
					?>
				</div>
				
			</div>
			
			<span class="line-v"></span>
			
		</div> <!-- page -->
		
		
		<div class="col-md-9 col-sm-9 col-xs-12 content-height">
			<div class="all-faqs" id="media-slide" style="display: block; overflow: hidden !important;">
		<?php		
			if( have_posts() ): while( have_posts() ): the_post();
			?>
					
				<div <?php post_class('media')?>>
					<div class="entry flexslider">
						<?php
						if( get_field('gallery')):
							?>
							<ul class="media-slides slides clearfix">
								<?php
								 while( has_sub_field('gallery') ):
								?>
								<li>
									<img src="<?php $image = get_sub_field('images'); echo $image["url"]; ?>"/>
								</li>
								<?php endwhile; ?>
							</ul>
						
						<?php
						endif;
						if ( get_field('gallery') == "" && has_post_thumbnail() ) {
							if( get_field('page_sub_title') ){
		        ?>
		        <h4 class="subtitle page-sub"><?php the_field( 'page_sub_title' ); ?></h4>
		        <?php
	        }
						 	echo aq_resizer_img('full', 900, 700);
						 }						
						?>
					</div>
				</div>						
			<?php
			endwhile; endif; ?>
		</div>
	</div>
	<?php
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();