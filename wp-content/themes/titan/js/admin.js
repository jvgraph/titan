(function($) {
	"use strict";
	
	$(function() {
		
		// Add the proper enctype to the form so the MP3 files can be uploaded
		$('form').attr( 'enctype', 'multipart/form-data' );
		
	});
	
}(jQuery));