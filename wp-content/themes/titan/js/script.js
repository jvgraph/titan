
(function($){
"use strict";
	$('body').removeClass('no-js');
	//hide or show sub menus
	$('.menu-item-has-children').children('a').on('click', function(event){
		event.preventDefault();
		$(this).toggleClass('submenu-open')
			   .next('.dropdown-menu')
			   .slideToggle(300).end()
			   .parent('.menu-item-has-children')
			   .siblings('.menu-item-has-children')
			   .children('a')
			   .removeClass('submenu-open')
			   .next('.dropdown-menu')
			   .slideUp(300);
	});
	
///////check the height of the vertical line dividing the content
////open external links in a new window
$('#slide-menu a').each(function() {
   var a = new RegExp('/' + window.location.host + '/');
   if(!a.test(this.href)) {
       $(this).click( function(event) {
           event.preventDefault();
           event.stopPropagation();
           window.open(this.href, '_blank');
       });
   }
});
////Video

//$("body.single-resources").tubular({videoId: '5joVKMvYO1Q', mute: false, repeat: false});

/****************************************************************************************************/
/*************************************************************************************/
var menu_trigger = $(".menu-trigger"),
	menu_close = $(".menu-trigger-close"),
	$body = $("body");

menu_trigger.click(function(){
	$body.addClass('menu-active');
	menu_close.delay(500).css({ opacity:1 });
});

menu_close.click(function(){
	$body.removeClass('menu-active');
});
$('#wrap, #footer').click(function(){
	$body.removeClass('menu-active');
});

/*************************************************************************************/
//FAQS PAGE 
/*************************************************************************************/

//hide all content below title
$(".faqs .entry").hide();
$( ".all-faqs, .tab.accordion" ).accordion({
      collapsible: true,
      heightStyle: "content"
});

////////////clear form

$('.sb-search-input').keyup(function(e){
	if(e.keyCode === 13){
	  $(this).trigger("enterKey");
	}
});
$('.sb-search-input').bind("enterKey",function(){
	$(this).val(" ");
});
if( !$("#sb-search").hasClass('open') ){
	$(".sb-search-input").val("");
}
///////////////////////

///lightbox

$('#work .gallery').featherlightGallery({
	gallery: {
		fadeIn: 300,
		fadeOut: 300
	},
	openSpeed:    300,
	closeSpeed:   300
});
//////////////////////////
///media slider
$('.flexslider').flexslider({
    animation: "fade",
    directionNav: false
  });
/////////////////
$(window).load(function(){
var catH = $('.content-height').outerHeight(),
	$bodyH = $('html').outerHeight();
	
	var newBodyH = $bodyH - 129;
	
	if ( catH <= $bodyH ){
		$('.line-v').height( newBodyH );
	}
	
	if ( catH  > $('.line-v').height() ){
	  $('.line-v').height( catH  );    
	} 
});
///// menu sub - parent page
$(".current_page_ancestor a.dropdown-toggle").addClass("submenu-open");
$(".current_page_ancestor a.dropdown-toggle.submenu-open ~ ul.dropdown-menu").css({display: 'block'});
///////////////////////////
///////// Slide Toggle child ul
    $('.col-sidebar li.menu-item-has-children > a, .col-sidebar li.page_item_has_children > a').on('click', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('up');
        $(this).next('ul').slideToggle();
    });

 /////////////	
	//end jquery
}(jQuery));
