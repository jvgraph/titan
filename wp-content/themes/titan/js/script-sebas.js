jQuery(document).ready(function($) {

    // Slide Toggle child ul
    $('.col-sidebar li.menu-item-has-children > a, .col-sidebar li.page_item_has_children > a').on('click', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('up');
        $(this).next('ul').slideToggle();
    });

    // Link to trigger history previous page
    $('.button.back').on('click', function () {
        history.go(-1);
    });

});