<?php
/**
 * Template Name: Company
**/

get_header(); 
locate_template(array('html-parts/html-before.php'), true, true);
?>

		<div class="col-sm-4 col-md-4 col-lg-3 col-xl-2 col-sidebar page">
            <header role="header" class="title">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>

            <?php if ((is_page(array('company', 'our-philosophy')))) { ?><!-- NOT CHILD -->
    			<div class="sub-nav">
					<?php
					    wp_nav_menu( array(
					        'menu'              => 'company_subnav',
					        'theme_location'    => 'company_subnav',
					        'depth'             => 2,
					        'container'         => '',
					        'menu_class'        => 'company_subnav',
					        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					        'walker'            => new wp_bootstrap_navwalker())
					    );
					?>
				</div>
            <?php } else { ?>
                <ul>
                <li class="about menu-item-has-children">
                    <a href="#">executive team</a>
                    <ul>
                    	<li class="panel">
                        <!-- CHILD OF EXECUTIVE TEAM -->
                        <?php if (is_page('careers') || 1474 == $post->post_parent) { ?>
                            <?php titan_page(); ?>
                        <?php } ?>
                        </li>
                    </ul>
                </li>
                <!-- CHILD OF PEOPLE IN TRANSIT -->
                <?php if ( is_page('our-people') || 1457 == $post->post_parent) { ?>
                    <?php titan_post_title(); ?>
                <?php } ?>

                <?php if ( is_page('careers') ) { ?>

                <li class="menu-item-has-children">
                    <a href="#">openings</a>
                    <ul>
                    	<li class="panel">
                            <ul>
                                <li>test</li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <?php } ?>

                </ul>

            <?php } ?>
			<span class="line-v"></span>
		</div><!-- .col-sidebar -->

		<div class="col-sm-8 col-md-8 col-lg-9 content-height">

            <?php if (is_page('executive-team')) { ?>
                <ul class="titan-thumbs">
                    <?php titan_people(); ?>
                </ul>
            <?php } if (is_page('careers') || 1474 == $post->post_parent) { ?><!-- CHILD OF EXECUTIVE TEAM -->

                <?php if ( has_post_thumbnail() ) { the_post_thumbnail('full-square'); } ?>

            <?php } else { ?><!-- NOT CHILD -->

                <?php if( get_field('page_sub_title') ){ ?>
                
                <div class="page-intro clearfix">
	                <div class="col-md-5 col-xs-12">
		                <h4 class="sub-title"><?php the_field( 'page_sub_title' ); ?></h4>
	                </div>
	                <div class="col-md-7 col-xs-12">
		                  <?php 
			                  if( have_posts() ): while( have_posts() ): the_post();
			                  the_content(); 
			                  endwhile; endif;
			                  ?>
	                </div>
                </div>
				<?php } ?>
	                
	              <div class="img-large clearfix"><?php  if ( has_post_thumbnail() ) { 
	              		              echo aq_resizer_img('full', 1100, 520); } ?>
	                              <?php 
	                
	                //titan_page(); ?></div>

            <?php } ?>

		</div><!-- MAIN -->

<?php 
	locate_template(array('html-parts/html-after.php'), true, true);
	get_footer(); ?>
