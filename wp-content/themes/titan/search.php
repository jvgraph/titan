<?php
get_header();

	locate_template(array('html-parts/html-before.php'), true, true);
	?>
	<div class="col-md-3 col-sm-3 col-xs-12 page">
			
			<div class="col-sidebar page-content">
				
				<header class="title">
					<h1>
						search results for: <?php echo get_search_query(); ?>
					</h1>
				</header>
				
			</div>
			
			<span class="line-v"></span>
			
		</div> <!-- page -->
		
		<div class="col-md-9 col-sm-9 col-xs-12">
						<div class="entry result">

<ul class="row">
	<?php		
	if( have_posts()): while( have_posts()): the_post();
	?>
	<li class="col-xs-12 col-sm-12 col-md-12">
		<h3>
			<a href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>
		</h3>
		<div class="entry-content">
			<?php the_excerpt(); ?>
		</div>
	</li>
	<?php
	if( have_rows('specs_files') ): 

		while( have_rows('specs_files') ): the_row(); 
		// vars
		$new_spec = get_sub_field('new_spec_file');
		$spec_title = get_sub_field('specs_file_name');
		$spec_link = get_sub_field('spec_file_url');
		?>
		<li class="col-xs-12 col-sm-12 col-md-12">
			<?php if( $new_spec == '' ): ?>
			<a href="<?php the_sub_field('spec_file_url'); ?>" target="_blank" download> 
				<?php the_sub_field('specs_file_name'); ?> <span class="icon icon-download"></span>
			</a>
			<?php else: ?>
			<a href="<?php the_sub_field('new_spec_file'); ?>" target="_blank" download> 
				<?php the_sub_field('specs_file_name'); ?> <span class="icon icon-download"></span>
			</a>
			<?php endif; ?>
		</li>

	<?php 
		endwhile; 
		endif; 
			
	endwhile; endif;
	?>
						</ul>
			</div>
	</div>
	<?php
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();