<?php

get_header();
	
	locate_template(array('html-parts/html-before.php'), true, true);
	
	if( have_posts() ): while( have_posts() ): the_post();

		get_template_part( PARTS, get_post_format( ) );
		
	endwhile; endif;
	
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();