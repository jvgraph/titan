<?php

get_header(); ?>

<div class="container-fluid">
	<div class="row">
		
		<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3" id="spec-nav">

            <div class="page-content col-sidebar"><header class="page-header" role="header">
                <h1 class="entry-title">
            	                <?php 
            		                echo empty( $post->post_parent ) ? 
            		                get_the_title( $post->ID ) : 		
            		                get_the_title( $post->post_parent );?> <?php the_title();  
            		            ?>
                </h1>
            </header>
            <!-- only for mobile -->
		<div class="visible-xs hidden-sm hidden-md hidden-lg">

            <div class="entry-meta">

           <ul class="list" id="specs">
	<?php 
		while( have_posts() ): the_post();
		
		if( have_rows('specs_files') ): 

		while( have_rows('specs_files') ): the_row(); 
		// vars
		$new_spec = get_sub_field('new_spec_file');
		$spec_title = get_sub_field('specs_file_name');
		$spec_link = get_sub_field('spec_file_url');
		?>
		<li class="spec-file">
			<?php if( $new_spec == '' ): ?>
			<a href="<?php the_sub_field('spec_file_url'); ?>" target="_blank" download> 
				<?php the_sub_field('specs_file_name'); ?> <span class="icon icon-download"></span>
			</a>
			<?php else: ?>
			<a href="<?php the_sub_field('new_spec_file'); ?>" target="_blank" download> 
				<?php the_sub_field('specs_file_name'); ?> <span class="icon icon-download"></span>
			</a>
			<?php endif; ?>
		</li>

	<?php 
		endwhile; 
		endif; 
	endwhile; ?>
	</ul>
            </div>

		</div><!-- MAIN -->
		<!-- ///////////////////// -->
            
            <?php if (is_archive('titan_prod_specs')) { ?>
                   JFIOMEOSMFOIE
            <?php } ?>
            
              <?php $args = array(
            	'child_of'     => 0,
            	'depth'        => 0,
            	'echo'         => 1,
            	'exclude'      => '',
            	'include'      => '',
            	'post_type'    => 'titan_prod_specs',
            	'post_status'  => 'publish',
            	'sort_column'  => 'menu_order, post_title',
                'sort_order'   => '',
            	'title_li'     => __(''),
            	'walker'       => ''
            ); ?>
            <div class="sub-nav">
            <ul>
                <?php wp_list_pages( $args ); ?>
            </ul>
            </div>
            	<span class="line-v"></span></div>
		</div><!-- .col-sidebar -->

		<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 hidden-xs">

            <div class="entry-meta">

           <ul class="list" id="specs">
	<?php 
		while( have_posts() ): the_post();
		
		if( have_rows('specs_files') ): 

		while( have_rows('specs_files') ): the_row(); 
		// vars
		$new_spec = get_sub_field('new_spec_file');
		$spec_title = get_sub_field('specs_file_name');
		$spec_link = get_sub_field('spec_file_url');
		?>
		<li class="spec-file">
			<?php if( $new_spec == '' ): ?>
			<a href="<?php the_sub_field('spec_file_url'); ?>" target="_blank" download> 
				<?php the_sub_field('specs_file_name'); ?> <span class="icon icon-download"></span>
			</a>
			<?php else: ?>
			<a href="<?php the_sub_field('new_spec_file'); ?>" target="_blank" download> 
				<?php the_sub_field('specs_file_name'); ?> <span class="icon icon-download"></span>
			</a>
			<?php endif; ?>
		</li>

	<?php 
		endwhile; 
		endif; 
	endwhile; ?>
	</ul>
            </div>

		</div><!-- MAIN -->

	</div><!-- .row -->
</div><!-- .container-fluid -->

<?php get_footer(); ?>
