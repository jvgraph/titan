<?php
get_header(); 

locate_template(array('html-parts/html-before.php'), true, true);
?>

		<div class="col-sm-12 col-sm-4 col-md-4 col-lg-3 col-xl-2 col-sidebar">

            <header role="header">
                <h1 class="entry-title"> <?php echo get_the_title(); ?></h1>
            </header>

            <div class="sub-nav"><ul>
            <?php $args = array(
                'authors'      => '',
                'child_of'     => 0,
                'depth'        => 0,
                'echo'         => 1,
                'exclude'      => '',
                'include'      => '',
                'post_type'    => 'titan_prod_specs',
                'post_status'  => 'publish',
                'sort_column'  => 'menu_order, post_title',
                'sort_order'   => '',
                'title_li'     => __(''),
                'walker'       => ''
            ); ?>
            
            <?php wp_list_pages( $args ); ?>
            
            </ul></div>
			<span class="line-v"></span>
		</div><!-- .col-sidebar -->

		<div class="col-sm-12 col-sm-8 col-md-8 col-lg-9 content-height" role="main">
<!--             <?php the_content(); ?> -->

		</div><!-- MAIN -->

<?php 
locate_template(array('html-parts/html-after.php'), true, true);

get_footer(); ?>
