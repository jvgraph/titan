</div> <!-- END OF WRAPPER -->
<div id="footer">
	<div class="container-fluid">
		<footer class="col-md-12 col-sm-12">
			<div class="row">
				<p class="site-info col-md-4 col-sm-4 col-xs-1">
					<span class="none">Copyright</span> 
					<span class="mobile-only">&copy;</span> 
					<span class="none"><?php echo date("Y"); ?> Titan Outdoor.</span> 
				</p>
				<div id="privacy-menu" class="col-md-8 col-sm-8 col-xs-11">
					<?php
				    wp_nav_menu( array(
				        'menu'              => 'privacy',
				        'theme_location'    => 'privacy',
				        'depth'             => 2,
				        'container'         => '',
				        'menu_class'        => 'nav navbar-nav privacy row',
				        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				        'walker'            => new wp_bootstrap_navwalker())
				    );
				?>
				</div>
			</div>
		</footer>
	</div>
</div>

<nav id="slide-menu">
		<h2 class="hide-text nav-title">site navigation</h2>
<div class="close-nav">
	<button type="button" class="navbar-toggle menu-trigger-close" data-toggle="collapse" data-target="#slide-menu">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" 
			xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			width="19.2px" height="19.4px" viewBox="0 0 19.2 19.4" enable-background="new 0 0 19.2 19.4" xml:space="preserve">
<path fill="#CCCCCC" d="M11.4,9.8l7.2-7.2c0.5-0.5,0.5-1.3,0-1.8c-0.5-0.5-1.3-0.5-1.8,0L9.6,8L2.4,0.8c-0.5-0.5-1.3-0.5-1.8,0
	c-0.5,0.5-0.5,1.3,0,1.8l7.2,7.2L0.6,17c-0.5,0.5-0.5,1.3,0,1.8c0.5,0.5,1.3,0.5,1.8,0l7.2-7.2l7.2,7.2c0.5,0.5,1.3,0.5,1.8,0
	c0.5-0.5,0.5-1.3,0-1.8L11.4,9.8z"/>
</svg>
		</button>
</div>
<?php
    wp_nav_menu( array(
        'menu'              => 'primary',
        'theme_location'    => 'primary',
        'depth'             => 2,
        'container'         => '',
        'menu_class'        => 'nav navbar-nav',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker())
    );
?>
</nav>

<?php wp_footer(); ?>
<?php  
echo '<script>new UISearch(document.getElementById("sb-search"));</script>';
//echo do_shortcode("[universal_video_player_and_bg settings_id='2']");;
?>
</body>
</html>