<?php
/*
*
* Template Name: news
*
*/
get_header();

	locate_template(array('html-parts/html-before.php'), true, true);
	?>
	

		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page">
			<div class="col-sidebar page-content">
				<header class="title"><h1><?php echo get_the_title(); ?></h1>
				</header>			
			</div>
			<?php
	
				$content = get_the_content();
				 ?>
				
			<div class="page-content">
		
			<?php if( $content == "" ){ ?>
		        <div class="tab accordion">
			        
		            <h2 class='collapser'>
			            overview
			        </h2>
		            
			        <div class="entry-content">
				        <div class="bio collapse-content">
			        		<?php 
				        		while(have_posts()): the_post();
						the_content();
						endwhile;
						wp_reset_postdata(); 
				        	?>
			        	</div>
			        </div>
			        
		        </div>
		        <?php } ?>
		        <div id="archive">
			        <h2>archive</h2>
			        <?php wp_get_archives('type=yearly'); ?>
		        </div>
		    </div>

			
			<span class="line-v"></span>
		</div>
		
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 content-height">
		<?php
			$args = array(
				'category_name' => 'news',
				'post_per_page' => 10
			);
			$news = new WP_Query($args);	
			if($news->have_posts()): while($news->have_posts()): $news->the_post();
			?>
			<article class="entry clearfix">
				<div class="news-post row">
					<div class="new-img col-md-3 col-xs-4">
						<a href="<?php the_permalink(); ?>"><?php 
						
							if( has_post_thumbnail()){
								echo aq_resizer_img('full', 200, 180);
							}
						?></a>
					</div>
					
					<div class="post-content col-md-9 col-xs-8"><h2 class="faqs-title">
						<a href="<?php the_permalink(); ?>">
						  <?php the_title(); ?>
						</a>
					</h2>
					<div class="content">
						 <?php the_excerpt(); ?>
					</div></div>
				</div>
			</article>
		  <?php endwhile; endif; ?>
		
		</div>
	<?php	

	
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();