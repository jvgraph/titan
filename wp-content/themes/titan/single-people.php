<?php 
get_header();
			
		get_template_part(HTML, 'before');
		
		if(have_posts()): while(have_posts()): the_post();
		
			get_template_part(PARTS, 'single');
			
		endwhile; endif;
		
		get_template_part(HTML, 'after');

get_footer(); 
?>