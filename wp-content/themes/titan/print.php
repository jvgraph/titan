<?php
/*
*
* Template Name: Titan Print
*
*/
get_header();

	locate_template(array('html-parts/html-before.php'), true, true);
	?>
	<div class="col-lg-3 col-md-4 col-sm-3 col-xs-12 page">
			
			<div class="col-sidebar page-content">
	<?php
		the_title('<h1 class="post-title">', '</h1>', true);
	?>				
				<div class="sub-nav">
					<?php
					    wp_nav_menu( array(
					        'menu'              => 'titan-print',
					        'theme_location'    => 'titan-print',
					        'depth'             => 2,
					        'container'         => '',
					        'menu_class'        => 'ask-titan-nav',
					        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					        'walker'            => new wp_bootstrap_navwalker())
					    );
					?>
				</div>
				
			</div>
			
			<span class="line-v"></span>
			
		</div> <!-- page -->
		
		
		<div class="col-lg-9 col-md-8 col-sm-9 col-xs-12 content-height">
			<ul class="row">

<?php	
	$args = array(
		'post_type' => 'page',
		'pagename' => 'titan-print',
	);
	$faqs = new WP_Query($args);
	?>
	<li <?php post_class('col-md-12')?>>
		<div class="entry">
			<?php 
			if ( has_post_thumbnail() ) {
				 echo aq_resizer_img('full', 800, 750);
				}
			?>
		</div>
	</li>
		</ul>
	</div>
	<?php
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();