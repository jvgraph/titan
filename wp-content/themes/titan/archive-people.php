<?php 
/*
*
* Template Name: People Page
*
*/
get_header();
			
		get_template_part(HTML, 'before');
		?>
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 page">
            <div class="page-content">
			<?php 
				$args1 = array(
					'page_id' => 1457,
					);
					$people_page = new WP_Query($args1);
				while( $people_page->have_posts() ): $people_page->the_post();?>
					<h1 class="title">executive team</h1>
					<div class="content-wrap">
						<!--
<div class="entry tab accordion">
							 <h2 class="collapser">about</h2>
					        <div class="collapse-content"><?php the_content(); ?></div>
						</div>
-->
					</div>
			<?php endwhile;
				wp_reset_postdata();
			?>
            </div>
 <span class="line-v"></span>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 staff content-height">
			<div class="row">
				<ul id="people" class="clearfix">
				<?php
					$args = array(
					'post_type' => 'people',
					'posts_per_page' => -1,
					'order' => 'ASC'				
					);
					$people = new WP_Query($args);
					
					if( $people->have_posts() ): while( $people->have_posts() ): $people->the_post(); ?>
					    <li <?php post_class("col-lg-3 col-md-3 col-sm-4 col-xs-6 person")?>>
							<div class="person-inner">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php
						if ( has_post_thumbnail() ) {
						 	echo aq_resizer_img('full', 360, 370);
						 }else{
							 echo get_the_attached_image();
						 }						
						?>
						<div class="person-info">
							<h3 class="name">
								<?php the_title();?>
								<span class="position">
								<?php $person_headline = esc_html(get_post_meta($post->ID, 'person_headline', true)); 
									if($person_headline != ''){
										echo $person_headline;
									}
								?>
								</span>
							</h3>
						</div>
						</a>
						</div>
						<?php						
					echo '</li>';
				
					endwhile; endif;
				?>
			</ul>
		</div>
	</div>
	<?php
	get_template_part(HTML, 'after');

get_footer(); 

?>