<?php
/*
*
* Template Name: FAQs
*
*/
get_header();

	locate_template(array('html-parts/html-before.php'), true, true);
	?>
	<div class="col-lg-3 col-md-4 col-sm-3 col-xs-12 page">
			
			<div class="col-sidebar page-content">
			<header class="title"><h1><?php the_title(); ?></h1></header>

	<?php
		
	?>
        <div class="entry tab accordion">
            <h2 class='collapser'>overview</h2>
	        <div class="entry-content">
		        <div class="bio collapse-content">
	        		<?php
		        		while(have_posts()): the_post();
						the_content();
						endwhile;
						wp_reset_postdata();
			        ?>
	        	</div>
	        </div>
        </div>
				
			</div>
			
			<span class="line-v"></span>
			
		</div> <!-- page -->
		
		
		<div class="col-lg-9 col-md-8 col-sm-9 col-xs-12 content-height">
			<ul class="all-faqs">
		<?php		
			$args = array(
				'post_type' => 'post',
				'category_name' => 'faqs',
				'posts_per_page' => -1
			);
			$faqs = new WP_Query($args);
			
			if($faqs->have_posts()): while($faqs->have_posts()): $faqs->the_post();
			?>
					
				<li <?php post_class('faqs')?>>
					<h2 class="faqs-title"><?php the_title(); ?></h2>
					<div class="entry">
						<?php the_content(); ?>
					</div>
				</li>					
				
			<?php
					
			endwhile; endif; ?>
		</ul>
	</div>
	<?php
	locate_template(array('html-parts/html-after.php'), true, true);
	
get_footer();